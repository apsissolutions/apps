/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : project_db

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-12-02 13:13:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dashboard_chart_blocks
-- ----------------------------
DROP TABLE IF EXISTS `dashboard_chart_blocks`;
CREATE TABLE `dashboard_chart_blocks` (
  `chart_blocks_id` int(10) NOT NULL AUTO_INCREMENT,
  `chart_blocks_name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `settings` text,
  `sql` text,
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`chart_blocks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dashboard_info_blocks
-- ----------------------------
DROP TABLE IF EXISTS `dashboard_info_blocks`;
CREATE TABLE `dashboard_info_blocks` (
  `info_blocks_id` int(10) NOT NULL AUTO_INCREMENT,
  `info_blocks_name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `settings` text,
  `sql` text,
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`info_blocks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dashboard_manage
-- ----------------------------
DROP TABLE IF EXISTS `dashboard_manage`;
CREATE TABLE `dashboard_manage` (
  `dashboard_manage_id` int(10) NOT NULL AUTO_INCREMENT,
  `dashboard_manage_name` varchar(250) NOT NULL,
  `layout` text NOT NULL,
  `editable_layout` text NOT NULL,
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`dashboard_manage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for dashboard_table_blocks
-- ----------------------------
DROP TABLE IF EXISTS `dashboard_table_blocks`;
CREATE TABLE `dashboard_table_blocks` (
  `table_blocks_id` int(10) NOT NULL AUTO_INCREMENT,
  `table_blocks_name` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `settings` text,
  `sql` text,
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`table_blocks_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


-- --------------------------------------------------
-- Dashboard Menu Generation
-- --------------------------------------------------

INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_url`, `menu_type`, `parent_menu_id`, `module_id`, `icon_class`, `sort_number`, `description`, `created_by`, `created`, `updated_by`, `updated`, `status`) VALUES (NULL, 'Dashboard Manager', '#', 'Main', '0', '1', 'fa fa-th', '0', '', '1', '2017-11-07 11:21:33', NULL, '2017-11-07 14:57:48', 'Active');

SET @last_id = LAST_INSERT_ID();

INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_url`, `menu_type`, `parent_menu_id`, `module_id`, `icon_class`, `sort_number`, `description`, `created_by`, `created`, `updated_by`, `updated`, `status`) VALUES (NULL, 'Chart', 'dashboard/chart/show', 'Sub', @last_id, '1', 'fa fa-line-chart', '0', 'chart add, edit and list', '1', '2017-11-08 04:27:14', NULL, '2017-11-22 12:57:46', 'Active');
INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_url`, `menu_type`, `parent_menu_id`, `module_id`, `icon_class`, `sort_number`, `description`, `created_by`, `created`, `updated_by`, `updated`, `status`) VALUES (NULL, 'Info Box', 'dashboard/infobox/show', 'Sub', @last_id, '1', 'fa fa-square-o', '0', '', '1', '2017-11-20 11:34:25', NULL, '2017-11-22 12:57:46', 'Active');
INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_url`, `menu_type`, `parent_menu_id`, `module_id`, `icon_class`, `sort_number`, `description`, `created_by`, `created`, `updated_by`, `updated`, `status`) VALUES (NULL, 'Table', 'dashboard/table/show', 'Sub', @last_id, '1', 'fa fa-table', '0', '', '1', '2017-11-22 12:56:30', NULL, '2017-11-22 12:57:51', 'Active');
INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_url`, `menu_type`, `parent_menu_id`, `module_id`, `icon_class`, `sort_number`, `description`, `created_by`, `created`, `updated_by`, `updated`, `status`) VALUES (NULL, 'Dashboard', 'dashboard/manage/show', 'Sub', @last_id, '1', 'fa fa-th-large', '0', '', '1', '2017-11-23 04:58:24', NULL, '2017-11-23 18:23:46', 'Active');