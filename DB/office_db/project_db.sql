/*
Navicat MySQL Data Transfer

Source Server         : Uvuvwevwevwe Onyetenyevwe Ugwemubwem Ossas
Source Server Version : 50625
Source Host           : 127.0.0.1:3306
Source Database       : project_db

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2017-10-12 19:04:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for doc
-- ----------------------------
DROP TABLE IF EXISTS `doc`;
CREATE TABLE `doc` (
  `doc_id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_brief` varchar(250) DEFAULT NULL,
  `doc_details` longtext,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`doc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doc
-- ----------------------------
INSERT INTO `doc` VALUES ('1', 'Debug  Tool', '<p>Use&nbsp;<span style=\"color:#FF0000;\"><strong>dd($data, true/false)&nbsp;</strong></span>to preview data for debugging</p>\r\n\r\n<p><strong>$data </strong>&nbsp; &nbsp;: &nbsp; &nbsp;Array / String<br />\r\n<strong>true</strong>&nbsp; &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp; Exit after printing<br />\r\n<strong>false</strong>&nbsp; &nbsp; &nbsp; : &nbsp; &nbsp; Continue after printing</p>\r\n', null, '2017-09-18 18:33:33', null, '2017-09-18 18:38:02', 'Active');
INSERT INTO `doc` VALUES ('3', 'Master Entry', '', null, '2017-09-18 18:28:41', null, '2017-09-18 18:38:00', 'Active');
INSERT INTO `doc` VALUES ('5', 'Flashdata', '<ul>\r\n	<li>Call a object function only for display flashdata on popup</li>\r\n	<li>use parameter for displaying theme anda message</li>\r\n</ul>\r\n\r\n<pre class=\"prettyprint\">\r\n<strong>$this-&gt;setFlashData(THEME_NAME, MESSAGE);\r\n</strong>\r\n// THEME_NAME : danger/info/success\r\n// MESSAGE    : Action has been executed Successfully\r\n</pre>\r\n', null, '2017-09-18 18:01:30', null, '2017-09-18 18:12:48', 'Active');
INSERT INTO `doc` VALUES ('6', 'Confirm Dialogue', '<ul>\r\n	<li>Use only for href link to use confirm action</li>\r\n</ul>\r\n\r\n<pre class=\"prettyprint\">\r\n<strong>&lt;a class=&quot;<span style=\"color:#FF0000;\">confirm-link</span>&quot; href=&quot;GIVEN_LINK&quot;&gt;&lt;i class=&quot;fa fa-close&quot;&gt;&lt;/i&gt;&lt;/a&gt;\r\n</strong>\r\n// confirm-link is for confirm dialogue\r\n// GIVEN_LINK : action url\r\n</pre>\r\n', null, '2017-09-18 18:05:41', null, '2017-09-18 18:06:51', 'Active');
INSERT INTO `doc` VALUES ('7', 'Dynamic Label Change', '<ul>\r\n	<li>After double click label can be changed if user wants</li>\r\n	<li>Use this kind of text to change the text</li>\r\n	<li>usnig &nbsp;&quot;<strong>labelchange</strong>&quot; class&nbsp;</li>\r\n</ul>\r\n\r\n<pre class=\"prettyprint\">\r\n&lt;span class=&quot;<strong>labelchange</strong>&quot; id=&quot;<span style=\"color:#FF0000;\">&lt;?= $base_name; ?&gt;</span>&quot;&gt;\r\n        <span style=\"color:#FF0000;\">&lt;?= defined($base_name) ? constant($base_name) : $base_name; ?&gt; </span>\r\n&lt;/span&gt;</pre>\r\n\r\n<p>&nbsp;</p>\r\n', null, '2017-09-18 18:19:39', null, null, 'Active');

-- ----------------------------
-- Table structure for dropdown
-- ----------------------------
DROP TABLE IF EXISTS `dropdown`;
CREATE TABLE `dropdown` (
  `dropdown_id` int(10) NOT NULL AUTO_INCREMENT,
  `dropdown_slug` varchar(50) DEFAULT NULL,
  `sqltext` text,
  `value_field` varchar(50) DEFAULT NULL,
  `option_field` varchar(100) DEFAULT NULL,
  `multiple` tinyint(1) DEFAULT '0',
  `dropdown_name` varchar(100) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`dropdown_id`),
  UNIQUE KEY `dropdownslug` (`dropdown_slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dropdown
-- ----------------------------
INSERT INTO `dropdown` VALUES ('1', 'user_level', 'SELECT user_level_id, user_level_name FROM user_level', 'user_level_id', 'user_level_name', '0', 'user_level_id', '', null, '2017-09-19 18:11:23', '1', '2017-10-09 16:19:10', 'Active');
INSERT INTO `dropdown` VALUES ('6', 'user_type', 'SELECT user_type_id, user_type_name FROM user_type', 'user_type_id', 'user_type_name', '0', 'user_type_id', '', '1', '2017-10-07 15:02:07', null, null, 'Active');
INSERT INTO `dropdown` VALUES ('7', 'dropdown_id', 'SELECT dropdown_id, dropdown_slug FROM dropdown', 'dropdown_id', 'dropdown_slug', '0', 'dropdown_id', '', '1', '2017-10-09 12:07:13', null, null, 'Active');
INSERT INTO `dropdown` VALUES ('8', 'dropdown_slug', 'SELECT dropdown_slug FROM dropdown', 'dropdown_slug', 'dropdown_slug', '0', 'dropdown_slug', '', '1', '2017-10-09 16:19:04', null, null, 'Active');

-- ----------------------------
-- Table structure for label_slug
-- ----------------------------
DROP TABLE IF EXISTS `label_slug`;
CREATE TABLE `label_slug` (
  `lang_slug` varchar(50) NOT NULL,
  `en_label` varchar(100) DEFAULT NULL,
  `bn_label` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`lang_slug`),
  UNIQUE KEY `slug` (`lang_slug`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of label_slug
-- ----------------------------
INSERT INTO `label_slug` VALUES ('dropdown_manager', 'Dropdown Manager', null);
INSERT INTO `label_slug` VALUES ('master_entry_table', 'Master Entry List', null);
INSERT INTO `label_slug` VALUES ('user_type', 'Test Slug', null);
INSERT INTO `label_slug` VALUES ('user_type_name', 'USER TYPE mm', null);

-- ----------------------------
-- Table structure for master_entry
-- ----------------------------
DROP TABLE IF EXISTS `master_entry`;
CREATE TABLE `master_entry` (
  `master_entry_table_name` varchar(50) NOT NULL,
  `field_name` varchar(50) NOT NULL,
  `input_type` varchar(30) NOT NULL COMMENT 'ex. text, combo, textarea etc',
  `required_field` varchar(10) NOT NULL,
  `sql` text,
  `comma_separated_value` text,
  `hide_from_grid` tinyint(1) NOT NULL DEFAULT '0',
  `hide_from_input` tinyint(1) NOT NULL,
  `self_filtering` tinyint(1) DEFAULT '0',
  `sorting` int(10) DEFAULT NULL,
  `label_name` varchar(50) NOT NULL,
  `placeholder` varchar(50) DEFAULT NULL,
  `field_key` varchar(10) NOT NULL,
  `created_by` int(10) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `join_with` varchar(50) DEFAULT NULL,
  `join_with_fk_name` varchar(50) DEFAULT NULL,
  `master_entry_title` varchar(50) DEFAULT NULL,
  `search` tinyint(1) DEFAULT '0',
  UNIQUE KEY `unique_master_entry` (`master_entry_table_name`,`field_name`,`master_entry_title`) USING BTREE,
  KEY `fk_me_title` (`master_entry_title`),
  CONSTRAINT `master_entry_ibfk_1` FOREIGN KEY (`master_entry_title`) REFERENCES `master_entry_table` (`master_entry_title`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of master_entry
-- ----------------------------
INSERT INTO `master_entry` VALUES ('module', 'description', 'text', '', '', '', '0', '0', '0', '3', 'DESCRIPTION', 'Type description', '', '0', '2014-12-07 15:18:17', null, '2017-09-12 15:41:43', 'Active', null, null, 'module', '0');
INSERT INTO `master_entry` VALUES ('module', 'home_url', 'var', '', '', '', '1', '0', '0', '2', 'Home Url', 'Home Url', '', '0', '2014-12-07 15:18:17', null, '2017-09-11 16:25:57', 'Active', null, null, 'module', '0');
INSERT INTO `master_entry` VALUES ('module', 'module_icon', 'var', 'required', '', '', '0', '0', '0', '1', 'Module Icon', 'Module Icon', '', '0', '2014-12-07 15:18:17', null, '2017-09-11 16:25:57', 'Active', null, null, 'module', '0');
INSERT INTO `master_entry` VALUES ('module', 'module_name', 'var', 'required', '', '', '0', '0', '0', '0', 'Module Name', 'Type Module Name', 'UNI', '0', '2014-12-07 15:18:17', null, '2017-09-11 16:25:53', 'Active', null, null, 'module', '0');
INSERT INTO `master_entry` VALUES ('module', 'status', 'select', 'required', '', 'Active,Inactive', '0', '0', '0', '4', 'Status', 'Status', '', '0', '2014-12-07 15:18:17', null, '2017-09-11 16:25:52', 'Active', null, null, 'module', '0');
INSERT INTO `master_entry` VALUES ('user_type', 'user_type_name', 'text', 'required', '', '', '0', '0', '1', '0', 'USER TYPE', 'User Type Name', '', '0', '2017-09-10 14:53:21', null, '2017-09-17 12:45:27', 'Active', null, null, 'user_type', '0');
INSERT INTO `master_entry` VALUES ('user_type', 'description', 'textarea', '', '', '', '0', '0', '0', '1', 'Description', 'Description', '', '0', '2017-09-10 14:53:21', null, '2017-09-12 15:01:56', 'Active', null, null, 'user_type', '0');
INSERT INTO `master_entry` VALUES ('user_type', 'status', 'select', '', '', 'Active,Inactive', '0', '0', '0', '2', 'Status', 'Status', '', '0', '2017-09-10 14:53:21', null, '2017-09-12 13:23:25', 'Active', null, null, 'user_type', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'user_level_name', 'text', 'required', '', '', '0', '0', '0', null, 'USER LEVEL NAME', 'Type user level name', 'UNI', '0', '2017-09-21 10:21:00', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'description', 'text', '', '', '', '0', '0', '0', null, 'DESCRIPTION', 'Type description', '', '0', '2017-09-21 10:21:00', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'status', 'select', 'required', '', 'Active', '0', '0', '0', null, 'STATUS', 'Type status', '', '0', '2017-09-21 10:21:00', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'privilege_edit', 'checkbox', '', '', '', '0', '0', '0', null, 'PRIVILEGE EDIT', 'Type privilege edit', '', '0', '2017-09-21 10:21:01', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'privilege_delete', 'checkbox', '', '', '', '0', '0', '0', null, 'PRIVILEGE DELETE', 'Type privilege delete', '', '0', '2017-09-21 10:21:01', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'privilege_add', 'checkbox', '', '', '', '0', '0', '0', null, 'PRIVILEGE ADD', 'Type privilege add', '', '0', '2017-09-21 10:21:01', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'privilege_view_all', 'checkbox', '', '', '', '0', '0', '0', null, 'PRIVILEGE VIEW ALL', 'Type privilege view all', '', '0', '2017-09-21 10:21:01', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('user_level', 'all_privilege', 'checkbox', '', '', '', '0', '0', '0', null, 'ALL PRIVILEGE', 'Type all privilege', '', '0', '2017-09-21 10:21:01', null, null, 'Active', null, null, 'user_level', '0');
INSERT INTO `master_entry` VALUES ('search_panel', 'search_panel_name', 'text', 'required', '', '', '0', '0', '0', null, 'Search Panel Name', 'Search Panel Name', '', '0', '2017-10-08 13:09:39', null, null, 'Active', null, null, 'search_panel', '0');
INSERT INTO `master_entry` VALUES ('search_panel', 'search_panel_slug', 'text', 'required', '', '', '0', '0', '0', null, 'Search Panel Slug', 'Search Panel Slug', 'UNI', '0', '2017-10-08 13:09:39', null, null, 'Active', null, null, 'search_panel', '0');
INSERT INTO `master_entry` VALUES ('search_panel', 'description', 'textarea', 'required', '', '', '0', '0', '0', null, 'Description', 'Description', '', '0', '2017-10-08 13:09:39', null, null, 'Active', null, null, 'search_panel', '0');
INSERT INTO `master_entry` VALUES ('search_panel', 'status', 'select', '', '', 'Active,Inactive', '0', '0', '0', null, 'Status', 'Status', '', '0', '2017-10-08 13:09:39', null, null, 'Active', null, null, 'search_panel', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'dropdown_slug', 'var', 'required', '', '', '0', '0', '0', null, 'Dropdown Slug', 'Dropdown Slug', 'UNI', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'sqltext', 'textarea', 'required', '', '', '0', '0', '0', null, 'Sqltext', 'Sqltext', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'value_field', 'var', 'required', '', '', '0', '0', '0', null, 'Value Field', 'Value Field', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'option_field', 'var', 'required', '', '', '0', '0', '0', null, 'Option Field', 'Option Field', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'multiple', 'checkbox', '', '', '', '0', '0', '0', null, 'Multiple', 'Multiple', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'dropdown_name', 'var', 'required', '', '', '0', '0', '0', null, 'Dropdown Name', 'Dropdown Name', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'description', 'textarea', '', '', '', '0', '0', '0', null, 'Description', 'Description', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('dropdown', 'status', 'select', '', '', 'Active', '0', '0', '0', null, 'Status', 'Status', '', '0', '2017-10-09 13:52:31', null, null, 'Active', null, null, 'dropdown_manager', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'master_entry_table_name', 'select', 'required', 'select table_name,table_name from information_schema.tables WHERE TABLE_SCHEMA=\"project_db\"', '', '0', '0', '0', null, 'MASTER ENTRY TABLE NAME', 'Type master entry table name', 'MUL', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '1');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'table_sql', 'tex', 'required', '', '', '1', '0', '0', null, 'TABLE SQL', 'Type table sql', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'form_column', 'text', '', '', '', '0', '0', '0', null, 'Form Column', 'Form Column', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'enable_details_view', 'checkbox', '', '', '', '1', '0', '0', null, 'ENABLE DETAILS VIEW ?', 'Type enable details view', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'disable_modal', 'checkbox', '', '', '', '0', '0', '0', null, 'Disable Modal', 'Disable Modal', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'template_details_view', 'text', '', '', '', '0', '0', '0', null, 'Details View Link', 'Link after base url', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'details_view_sql', 'text', '', '', '', '1', '0', '0', null, 'DETAILS VIEW SQL', 'Type details view sql', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'status', 'select', '', '', 'Active,Inactive', '1', '0', '0', null, 'Status', 'Status', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '1');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'master_entry_title', 'var', 'required', '', '', '0', '0', '0', null, 'MASTER ENTRY TITLE', 'Use lowercase and no space', 'UNI', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'disable_grid_view', 'checkbox', '', '', '', '0', '0', '0', null, 'DISABLE GRID VIEW ?', 'Disable Grid View', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'disable_form_view', 'checkbox', '', '', '', '0', '0', '0', null, 'DISABLE FORM VIEW ?', 'Disable Form View', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');
INSERT INTO `master_entry` VALUES ('master_entry_table', 'include_search_panel', 'checkbox', '', '', '', '0', '0', '0', null, 'INCLUDE SEARCH PANEL ?', 'Include Search Panel', '', '0', '2017-10-09 18:24:41', null, null, 'Active', null, null, 'master_entry_table', '0');

-- ----------------------------
-- Table structure for master_entry_table
-- ----------------------------
DROP TABLE IF EXISTS `master_entry_table`;
CREATE TABLE `master_entry_table` (
  `master_entry_table_id` int(10) NOT NULL AUTO_INCREMENT,
  `master_entry_table_name` varchar(50) NOT NULL,
  `table_sql` text NOT NULL COMMENT 'possible select query to read this table',
  `form_column` int(1) DEFAULT '1',
  `enable_details_view` tinyint(1) DEFAULT '0',
  `disable_modal` tinyint(1) DEFAULT '0',
  `template_details_view` varchar(50) DEFAULT NULL COMMENT 'the template name of the view which will pop up to show',
  `details_view_sql` text,
  `created_by` int(10) NOT NULL DEFAULT '0',
  `created` timestamp NULL DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `master_entry_title` varchar(50) DEFAULT NULL COMMENT 'Use lower case and no space',
  `where_clause_filter` text,
  `export_excel` tinyint(1) DEFAULT '0',
  `export_pdf` tinyint(1) DEFAULT '0',
  `export_csv` tinyint(1) DEFAULT '0',
  `enable_printing` tinyint(1) DEFAULT '0',
  `file_upload_path` varchar(250) DEFAULT NULL,
  `import_excel` tinyint(1) DEFAULT '0',
  `disable_grid_view` tinyint(1) DEFAULT '0',
  `disable_form_view` tinyint(1) DEFAULT '0',
  `include_search_panel` tinyint(1) DEFAULT '0',
  `disable_action` tinyint(1) DEFAULT '0',
  `disable_edit_icon` tinyint(1) DEFAULT '0',
  `disable_delete_icon` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`master_entry_table_id`),
  UNIQUE KEY `u_master_entry_title` (`master_entry_title`),
  KEY `u_table_name` (`master_entry_table_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of master_entry_table
-- ----------------------------
INSERT INTO `master_entry_table` VALUES ('1', 'master_entry_table', 'SELECT master_entry_table_id,form_column,master_entry_table_name,table_sql, template_details_view, enable_details_view,disable_modal,master_entry_title,export_pdf,export_csv,export_excel,enable_printing,disable_grid_view,disable_form_view,include_search_panel FROM master_entry_table', '1', '0', '0', '', '', '0', '2014-12-07 15:13:30', '1', '2017-10-09 18:42:55', 'Active', 'master_entry_table', '', '0', '1', '0', '0', '', '0', '0', '0', '1', '0', '0', '0');
INSERT INTO `master_entry_table` VALUES ('2', 'user_level', 'SELECT user_level.*\r\nFROM user_level WHERE user_level.user_level_id NOT IN (1,12)', '1', '0', '0', '', '', '0', '2014-12-07 15:13:30', '1', '2017-09-10 17:38:24', 'Active', 'user_level', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `master_entry_table` VALUES ('3', 'user_type', 'SELECT user_type.* FROM user_type', '1', '0', '0', '', '', '0', '2014-12-07 15:13:30', '1', '2017-04-09 20:11:53', 'Active', 'user_type', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `master_entry_table` VALUES ('4', 'module', 'SELECT module.module_id, \n	module.module_name, \n	module.module_icon, \n	module.description, \n	module.home_url, \n	module.`status`\nFROM module', '1', '0', '0', '', '', '1', '2014-12-07 15:13:30', null, '2017-09-21 11:35:34', 'Active', 'module', '', '0', '0', '0', '0', '', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `master_entry_table` VALUES ('5', 'dropdown', 'SELECT * FROM dropdown', '1', '1', '0', 'dropdown_manager/dropdown/dropdown_sample', '', '1', '2017-09-20 18:53:49', '1', '2017-10-07 13:46:11', 'Active', 'dropdown_manager', null, '0', '0', '0', '0', null, '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `master_entry_table` VALUES ('6', 'search_panel', 'SELECT * FROM search_panel', '1', '1', '1', 'grid_manager/griding/search_panel_view', '', '1', '2017-10-08 12:32:30', '1', '2017-10-09 18:54:50', 'Active', 'search_panel', null, '0', '0', '0', '0', null, '0', '0', '0', '1', '0', '0', '0');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(100) DEFAULT NULL,
  `menu_type` enum('Main','Sub') NOT NULL,
  `parent_menu_id` int(10) DEFAULT NULL,
  `module_id` int(10) DEFAULT NULL,
  `icon_class` varchar(50) DEFAULT NULL,
  `sort_number` int(2) NOT NULL DEFAULT '0',
  `description` text,
  `created_by` int(10) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`menu_id`),
  KEY `fk_module_id` (`module_id`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `module` (`module_id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Master Entry', 'master/show_master/master_entry_table', 'Main', '0', '1', 'glyphicon glyphicon-link', '1', null, '0', '2017-04-10 12:35:13', null, '2017-09-17 16:24:18', 'Active');
INSERT INTO `menu` VALUES ('2', 'Create Master Entry', 'master', 'Sub', '1', '1', 'fa fa-list', '1', '', '1', '2017-09-17 12:15:16', null, '2017-09-17 16:03:31', 'Active');
INSERT INTO `menu` VALUES ('3', 'Menu Manager', '', 'Main', '0', '1', 'fa fa-bars', '3', null, '0', '2017-04-09 20:13:40', null, '2017-10-08 15:33:08', 'Active');
INSERT INTO `menu` VALUES ('4', 'Create Menu', 'menu_manager/menu/menu_list', 'Sub', '3', '1', 'fa fa-th', '1', '', '0', '2017-04-09 20:13:40', null, '2017-09-03 03:05:08', 'Active');
INSERT INTO `menu` VALUES ('5', 'Add User', 'master/show_master/user', 'Sub', '9', '1', 'fa fa-plus', '2', null, '0', '2017-04-09 20:13:40', null, '2017-09-21 11:19:50', 'Active');
INSERT INTO `menu` VALUES ('6', 'User Level', 'master/show_master/user_level', 'Sub', '9', '1', 'glyphicon glyphicon-tags', '3', 'Create Level and Set privilege access permission for any particular Level.', '0', '2017-04-09 20:13:40', null, '2017-09-21 11:19:50', 'Active');
INSERT INTO `menu` VALUES ('7', 'Menu Privilege for Level', 'menu_manager/menu/get_menu_for_level', 'Sub', '3', '1', 'glyphicon glyphicon-star-empty', '2', '', '0', '2017-04-09 20:13:40', null, '2017-09-03 03:05:23', 'Active');
INSERT INTO `menu` VALUES ('9', 'User Manager', '#', 'Main', '0', '1', 'glyphicon glyphicon-user', '6', null, '0', '2017-04-09 20:13:40', null, '2017-10-08 15:33:08', 'Active');
INSERT INTO `menu` VALUES ('10', 'DOCUMENTATION', '#', 'Main', '0', '1', 'fa fa-list', '7', '', '1', '2017-09-17 04:01:43', null, '2017-10-08 15:33:08', 'Inactive');
INSERT INTO `menu` VALUES ('11', 'Create New', 'home/documentation/new', 'Sub', '10', '1', 'fa fa-list', '1', '', '1', '2017-09-17 04:01:43', null, '2017-09-19 16:43:23', 'Inactive');
INSERT INTO `menu` VALUES ('12', 'See Doc', 'home/documentation', 'Sub', '10', '1', 'fa fa-list', '2', '', '1', '2017-09-17 04:01:43', null, '2017-09-19 16:43:26', 'Inactive');
INSERT INTO `menu` VALUES ('13', 'Module', 'master/show_master/module', 'Main', '0', '1', 'fa fa-puzzle-piece', '4', null, '0', '2017-04-09 20:13:40', null, '2017-10-08 15:33:08', 'Active');
INSERT INTO `menu` VALUES ('16', 'Create Master Table', 'master/create_master_table', 'Sub', '1', '1', 'fa fa-table', '2', null, '0', '2017-04-09 20:13:40', null, '2017-09-17 16:03:32', 'Active');
INSERT INTO `menu` VALUES ('17', 'User Type', 'master/show_master/user_type', 'Sub', '9', '1', 'fa fa-user', '1', null, '1', '2016-04-19 15:43:16', null, '2017-09-21 11:19:50', 'Active');
INSERT INTO `menu` VALUES ('19', 'Master entry table', 'master/show_master/master_entry_table', 'Sub', '1', '1', 'fa fa-list', '3', null, '1', '2017-04-09 11:29:39', null, '2017-09-17 16:03:32', 'Active');
INSERT INTO `menu` VALUES ('24', 'Static Content', 'admin/admin/static_content', 'Main', '0', '2', 'fa fa-file-text-o', '2', '', '1', '2017-09-02 07:19:16', null, '2017-09-03 03:38:11', 'Active');
INSERT INTO `menu` VALUES ('25', 'Dropdown Manager', 'master/show_master/dropdown_manager', 'Main', '0', '1', 'fa fa-leaf', '5', '', '1', '2017-09-21 11:18:19', null, '2017-10-08 15:33:08', 'Active');
INSERT INTO `menu` VALUES ('26', 'Grid Manager', '#', 'Main', '0', '1', 'fa fa-list', '2', '', '1', '2017-10-08 02:53:06', null, '2017-10-08 15:33:08', 'Active');
INSERT INTO `menu` VALUES ('27', 'Search Panel', 'master/show_master/search_panel', 'Sub', '26', '1', 'fa fa-search', '1', '', '1', '2017-10-08 02:53:34', null, '2017-10-08 14:54:34', 'Active');

-- ----------------------------
-- Table structure for module
-- ----------------------------
DROP TABLE IF EXISTS `module`;
CREATE TABLE `module` (
  `module_id` int(10) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_icon` varchar(100) DEFAULT NULL,
  `description` text,
  `home_url` varchar(100) DEFAULT NULL,
  `created_by` int(10) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `module_name` (`module_name`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of module
-- ----------------------------
INSERT INTO `module` VALUES ('1', 'App Config', 'fa fa-cogs', 'All kind of master entry settings for any module', '', '1', '2014-09-24 17:41:28', '1', '2017-08-30 02:11:50', 'Active');
INSERT INTO `module` VALUES ('2', 'Admin', 'fa fa-shield', 'A great way to develop CMS easily.', '', '1', '2014-09-24 17:41:26', '1', '2017-08-30 02:11:15', 'Active');

-- ----------------------------
-- Table structure for privilege_level
-- ----------------------------
DROP TABLE IF EXISTS `privilege_level`;
CREATE TABLE `privilege_level` (
  `user_id` int(10) DEFAULT NULL,
  `user_level_id` int(10) DEFAULT NULL,
  UNIQUE KEY `u_entry` (`user_id`,`user_level_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of privilege_level
-- ----------------------------
INSERT INTO `privilege_level` VALUES ('2', '1');

-- ----------------------------
-- Table structure for privilege_menu
-- ----------------------------
DROP TABLE IF EXISTS `privilege_menu`;
CREATE TABLE `privilege_menu` (
  `menu_id` int(10) NOT NULL,
  `user_level_id` int(10) DEFAULT NULL,
  UNIQUE KEY `u_privilege_menu` (`menu_id`,`user_level_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of privilege_menu
-- ----------------------------
INSERT INTO `privilege_menu` VALUES ('24', '1');

-- ----------------------------
-- Table structure for privilege_module
-- ----------------------------
DROP TABLE IF EXISTS `privilege_module`;
CREATE TABLE `privilege_module` (
  `user_id` int(10) NOT NULL,
  `user_module_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of privilege_module
-- ----------------------------
INSERT INTO `privilege_module` VALUES ('2', '2');

-- ----------------------------
-- Table structure for privilege_user
-- ----------------------------
DROP TABLE IF EXISTS `privilege_user`;
CREATE TABLE `privilege_user` (
  `user_id` int(11) DEFAULT NULL,
  `priv_menu` int(11) DEFAULT NULL,
  `ex_priv_menu` int(11) DEFAULT NULL,
  KEY `u_entry` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of privilege_user
-- ----------------------------

-- ----------------------------
-- Table structure for search_panel
-- ----------------------------
DROP TABLE IF EXISTS `search_panel`;
CREATE TABLE `search_panel` (
  `search_panel_id` int(10) NOT NULL AUTO_INCREMENT,
  `search_panel_name` varchar(100) NOT NULL,
  `search_panel_slug` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`search_panel_id`),
  UNIQUE KEY `SearchPanel` (`search_panel_slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of search_panel
-- ----------------------------
INSERT INTO `search_panel` VALUES ('1', 'Search Panel One', 'search_panel_one', 'werwer', '1', '2017-10-08 13:10:31', '1', '2017-10-09 17:36:07', 'Active');

-- ----------------------------
-- Table structure for search_panel_details
-- ----------------------------
DROP TABLE IF EXISTS `search_panel_details`;
CREATE TABLE `search_panel_details` (
  `search_panel_details_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `search_panel_id` int(10) NOT NULL,
  `label_name` varchar(50) DEFAULT NULL,
  `placeholder_name` varchar(50) DEFAULT NULL,
  `input_type` enum('text','dropdown','range','dropdown_range') DEFAULT 'text',
  `input_text_type` enum('number','email','text','url') DEFAULT 'text',
  `dropdown_slug` varchar(50) DEFAULT NULL,
  `dropdown_multiselect` tinyint(1) DEFAULT '0',
  `input_name` varchar(50) DEFAULT NULL,
  `input_class` varchar(100) DEFAULT 'form-control',
  `input_id` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`search_panel_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of search_panel_details
-- ----------------------------
INSERT INTO `search_panel_details` VALUES ('1', '1', 'First Name', 'First name', 'text', 'number', '', '0', 'first_name', 'form-control', '', '', null, '2017-10-11 14:22:21', null, '2017-10-11 15:29:39', 'Active');
INSERT INTO `search_panel_details` VALUES ('2', '1', 'Dropdown Range', '', 'dropdown', 'text', 'dropdown_id', '0', 'dropdown_number', 'form-control', '', '', null, '2017-10-11 14:52:19', null, '2017-10-11 15:44:06', 'Active');
INSERT INTO `search_panel_details` VALUES ('3', '1', 'asdasd', '', 'range', 'number', '', '0', 'asd', 'form-control', '', '', null, '2017-10-11 18:54:22', null, null, 'Active');
INSERT INTO `search_panel_details` VALUES ('4', '1', 'qwe', 'qwe', 'dropdown', 'text', 'user_level', '1', 'qwe', 'form-control', 'qwe', '', null, '2017-10-12 16:57:31', null, null, 'Active');
INSERT INTO `search_panel_details` VALUES ('5', '1', 'Dropdown slug range', '', 'dropdown_range', 'text', 'dropdown_slug', '1', 'dropdown_slug', 'form-control', '', '', null, '2017-10-12 18:57:52', null, null, 'Active');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT 'e10adc3949ba59abbe56e057f20f883e' COMMENT 'encoded in md5 format',
  `secret_question_1` varchar(100) DEFAULT NULL,
  `secret_question_2` varchar(100) DEFAULT NULL,
  `secret_question_ans_1` varchar(100) DEFAULT NULL,
  `secret_question_ans_2` varchar(100) DEFAULT NULL,
  `identification_expire_date` date DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `religion_id` enum('Muslim','Hindu','Christian','Buddhist') DEFAULT 'Muslim',
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `last_login_date_time` timestamp NULL DEFAULT NULL,
  `default_module_id` int(10) DEFAULT '3',
  `created_by` int(10) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `user_image` varchar(100) DEFAULT 'images/default/avatar.jpg',
  `address` varchar(100) DEFAULT NULL,
  `default_url` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `u_username` (`username`) USING BTREE,
  UNIQUE KEY `u_email` (`email`) USING BTREE,
  KEY `fk_religion` (`religion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Admin', 'Super', 'admin@we.com', '01755557265', 'sadmin', 'e10adc3949ba59abbe56e057f20f883e', 'What is your first School Name?', 'Which city you born?', 'Uttar Digholy', 'Munshiganj', null, '1983-03-08', 'Male', 'Muslim', 'Serajul Islam', 'Fatema Islam', '0000-00-00 00:00:00', '1', '0', '2014-12-09 06:26:45', '1', '2017-08-16 14:43:15', 'Active', 'images/default/avatar.jpg', null, '');
INSERT INTO `user` VALUES ('2', '', '', '', '', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '', null, '0000-00-00', 'Male', '', '', '', null, '2', '1', '2017-04-10 17:05:34', '1', '2017-09-10 12:25:20', 'Active', 'images/users/2.jpg', 'khulna', '');

-- ----------------------------
-- Table structure for user_level
-- ----------------------------
DROP TABLE IF EXISTS `user_level`;
CREATE TABLE `user_level` (
  `user_level_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_level_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(10) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  `privilege_edit` tinyint(1) NOT NULL DEFAULT '0',
  `privilege_delete` tinyint(1) NOT NULL DEFAULT '0',
  `privilege_add` tinyint(1) NOT NULL DEFAULT '0',
  `privilege_view_all` tinyint(1) NOT NULL DEFAULT '0',
  `all_privilege` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_level_id`),
  UNIQUE KEY `u_level_name` (`user_level_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_level
-- ----------------------------
INSERT INTO `user_level` VALUES ('1', 'Admin', 'Admin previlege for all module.', '1', '2014-08-31 12:06:38', '1', '2014-11-16 15:25:33', 'Active', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for user_type
-- ----------------------------
DROP TABLE IF EXISTS `user_type`;
CREATE TABLE `user_type` (
  `user_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(250) NOT NULL,
  `description` text,
  `created_by` int(10) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(10) DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Inactive') DEFAULT 'Active',
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_type
-- ----------------------------
INSERT INTO `user_type` VALUES ('1', 'Admin', null, '1', '2016-04-19 13:14:57', null, null, 'Active');
