/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    config.skin = 'flat';
    config.protectedSource.push(/<i[^>]*><\/i>/g);
    config.allowedContent = true;
    config.toolbarGroups = [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		// { name: 'styles', groups: [ 'styles' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'document', groups: [ 'document', 'doctools', 'mode' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
    // config.uiColor = '#b2d3d7';
    // config.extraPlugins = codeTag';
    config.extraPlugins = 'insertpre';

	config.removeButtons = 'Flash,Undo,Redo,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Anchor,Unlink,' +
        'Language,BidiRtl,BidiLtr,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,' +
        'Checkbox,Scayt,SelectAll,Find,Replace,Cut,Copy,Paste,PasteText,PasteFromWord,Templates,Save,' +
        'NewPage,Preview,Print,About,ShowBlocks,Superscript,Subscript,CreateDiv,RemoveFormat,Styles';
};
CKEDITOR.on('dialogDefinition', function(ev) {
    // Take the dialog name and its definition from the event data.
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    // Check if the definition is from the dialog we're
    // interested in (the 'link' dialog).
    if (dialogName == 'link') {
//        Remove the 'Upload' and 'Advanced' tabs from the 'Link' dialog.
//        dialogDefinition.removeContents('upload');
//        dialogDefinition.removeContents('advanced');

        // Get a reference to the 'Link Info' tab.
        var infoTab = dialogDefinition.getContents('info');
        // Remove unnecessary widgets from the 'Link Info' tab.
        infoTab.remove('linkType');
        infoTab.remove('protocol');
        infoTab.remove('browse');

        // Get a reference to the "Target" tab and set default to '_blank'
        var targetTab = dialogDefinition.getContents('target');
        var targetField = targetTab.get('linkTargetType');
        targetField['default'] = '_blank';

    } else if (dialogName == 'image') {
        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');
        var infoTab = dialogDefinition.getContents('info');
        infoTab.remove('browse');
        infoTab.remove('txtHSpace');
        infoTab.remove('txtVSpace');
        infoTab.remove('txtBorder');
        infoTab.remove('cmbAlign');
    }
});