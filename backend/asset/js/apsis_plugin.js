$(document).ready(function(){
    webshim.setOptions("forms-ext", {
        "widgets": { "startView": 2, "minView": 0, "inlinePicker": false, "size": 1, "splitInput": false, "yearSelect": false, "monthSelect": false, "daySelect": false, "noChangeDismiss": false, "openOnFocus": false, "buttonOnly": true, "classes": "", "popover": { }, "calculateWidth": true, "animate": true, "toFixed": 0, "onlyFixFloat": false }});
    webshims.polyfill('forms forms-ext');
    $('p.money').bdt();
    /*----------------------MULTI-SELECT OPTION------------------------*/
    $('.multi').multiselect({
        includeSelectAllOption: true,
        nonSelectedText: 'Choose an option',
        buttonClass: 'form-control',
        buttonWidth: '100%'
    });
    $(".caret").css('float', 'right');
    $(".caret").css('margin', '8px 0');
    $(".multiselect-selected-text").css('float', 'left');
    $(".multiselect-selected-text").css('margin', '0 5px');
    /*----------------- Datetime picker ---------------------*/
    $('.datetimepicker').datetimepicker({ weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0, showMeridian: 1 });
    /*--------Class for only date picker----------*/
    $('.datepicker').datetimepicker({ weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 2, minView: 2, forceParse: 0, format: 'yyyy-mm-dd' });
    /*--------Class for only month picker------------*/
    $('.monthpicker').datetimepicker({ weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 3, minView: 3, forceParse: 0 });
    /*----------Class for only time picker------*/
    $('.timepicker').datetimepicker({ weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: 1, startView: 1, minView: 0, maxView: 1, forceParse: 0 });
    /*----------------Date Range Picker -------------------*/
    $('.dtrange').daterangepicker({
        "autoApply": true,
        "locale": {
            format: 'YYYY-MM-DD'
        }
    });

    /*/ --------------- Datepicker related code ends ----------------- /*/
    $("#selectAll").click(function (){
        if(this.checked){
            $('#dataTables-checkbox tbody tr').each(function(){
                if($(this).children('td').first().children('input').length){
                    $('.chkbox:visible').prop('checked',true);
                    $(this).addClass('new_tr');
                }
            });
        }else{
            $('#dataTables-checkbox tbody tr').each(function(){
                if($(this).children('td').first().children('input').length){
                    $('.chkbox:visible').prop('checked',false);
                    $(this).removeClass('new_tr');
                }
            });
        }
    });
    $('.chkbox').click(function(){
        if(this.checked){
            $(this).parent('td').parent('tr').addClass('new_tr');
        }else{
            $(this).parent('td').parent('tr').removeClass('new_tr');
        }
    });
});

function stopPropagation(evt) {
    if (evt.stopPropagation !== undefined) {
        evt.stopPropagation();
    } else {
        evt.cancelBubble = true;
    }
}
function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
function apsis_money(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2 + '.00';
}
(function ( $ ) {
    $.fn.bdt = function( options ){
        var nStr = this.text();
        nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
        var settings = $.extend({
            bdt_sign: 'BDT'
        }, options );
        return this.text(x1 + x2 + '.00'+settings.bdt_sign);
    };
    
    $.fn.calculate = function(options){
        var total = 0;
        this.each(function(){
            total = total + parseInt($(this).text());
        });
        
        var settings = $.extend({
            target_id: 'total',
            money:true
        });
        if($('#' + settings.target_id).length == 0){
            return alert('Sorry no target id defined to draw result.');
        }
        $('#' + settings.target_id).text(total)
        if(settings.money){
            $('#' + settings.target_id).bdt();
        }
    }
}(jQuery));
/**========================CONFIRMATION SCRIPT===================**/
$(".confirm-link").on("click", function(e){
    e.preventDefault();
    var link = $(this).attr('href');
    var message = $(this).data('message') ? $(this).data('message') : "Are you Confirm ? This cannot be undone.";
    var yes = $(this).data('yes') ? $(this).data('yes') : "Confirm";
    var no = $(this).data('no') ? $(this).data('no') : "No";
    bootbox.confirm({
//            title: "Confirmation Dialogue",
        message: message,
        buttons: {
            cancel: {
                className: 'btn-danger',
                label: '<i class="fa fa-times"></i> ' + no
            },
            confirm: {
                className: 'btn-success',
                label: '<i class="fa fa-check"></i> ' + yes
            }
        },
        callback: function (result) {
            if(result === true){
                window.location.href = link;
            }
        }
    });
});
/**========================END CONFIRMATION SCRIPT===================**/
/**========================Label Change SCRIPT===================**/
$(".labelchange").on("click", function(e){
    // alert('ssss');
    var labelid = $(this).attr('id');
    $.fn.editable.defaults.mode = 'inline';
    $('#'+labelid).editable({
        type: 'text',
        pk: 1,
        url: BASE_URL+"master/labelChange/label",
        // url: "<?php echo base_url('master/labelChange/label'); ?>",
        title: 'Change Label Text',
        clear: false,
        validate: function(value) {
            if($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
});
/**========================END Label Change SCRIPT===================**/
/**========================Ajax call SCRIPT===================**/
function ajax_call(func_name,val,field_name,conditional_field,id_field){
    var on_change = $('#'+id_field).data('allow_ajax');
    $.ajax({
        url:'../master/generate_combo_by_ajax_call',
        type:"post",
        data:{function_name:func_name,field_name:field_name,id:conditional_field,val:val,on_change:on_change},
        success:function(data){
            $('#'+id_field).html(data);
            $('select').select2();
        }
    });
}
/*------------ Update CK Editor Content ------------*/
function updateCKeditorContent(){
    for ( instance in CKEDITOR.instances ){
        CKEDITOR.instances[instance].updateElement();
    }
    /*------------ Call this function before ajax call ------------*/
}
/*----------------------CK EDITOR FUNCTION------------------------*/
$(document).ready(function(){
    var cck = 1;
    $(".ckeditor").each(function(){
        $(this).attr("id","ckeditor"+cck);
        CKEDITOR.replace( 'ckeditor'+cck);
        cck = cck + 1;
    });
    updateCKeditorContent();
});
/*----------------------CK EDITOR FUNCTION------------------------*/
/*------------End Update CK Editor Content ------------*/