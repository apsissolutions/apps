<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: nijhu
 * Date: 05-Mar-17
 * Time: 9:53 PM
 */
$config['header_css'] = array(
    'css/bootstrap.min.css',
    'dist/css/AdminLTE.min.css',
    'dist/css/skins/_all-skins.min.css',
    'plugins/datepicker/datepicker3.css',
    'plugins/daterangepicker/daterangepicker-bs3.css',
    'css/bootstrap-datetimepicker.min.css',
    'plugins/datatables/dataTables.bootstrap.css',
    'plugins/datatables/dataTables.tableTools.css',
    'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css',
    'css/bootstrap-multiselect.css',
    'plugins/pace/pace.min.css',
    'plugins/bootstrap-x-editable/bootstrap-editable.css'
);
$config['header_js']  = array(
    'plugins/jQuery/jQuery-2.2.0.min.js',
    'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
    'js/bootstrap.min.js',
    'js/bootstrap-multiselect.js',
    'js/bootstrap-datetimepicker.min.js',
    'js/bootbox.min.js',
    'https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js',
    'https://oss.maxcdn.com/respond/1.4.2/respond.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js',
    'plugins/daterangepicker/daterangepicker.js',
    'plugins/datepicker/bootstrap-datepicker.js',
    'plugins/slimScroll/jquery.slimscroll.min.js',
    'plugins/fastclick/fastclick.js',
    'dist/js/app.min.js',
    'plugins/bootstrap-x-editable/bootstrap-editable.min.js',
    'js/validator.js',
    //'https://code.highcharts.com/highcharts.js'

);
$config['footer_css'] = array();
$config['footer_js']  = array(
    'dist/js/admin_theme_settings.js',
    'plugins/datatables/jquery.dataTables.js',
    'plugins/datatables/dataTables.bootstrap.js',
    'plugins/datatables/dataTables.tableTools.js',
    'js/custom_dataTable.js',
    'https://cdn.datatables.net/fixedcolumns/3.2.2/js/dataTables.fixedColumns.min.js',
    'js/apsis_plugin.js',
    'https://cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js',
    'plugins/pace/pace.min.js'

);

$config['footer_scripts']  = '';