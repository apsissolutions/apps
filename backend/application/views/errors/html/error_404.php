<div class="col-md-8 col-md-offset-2">
<!--    <h1 class="headline text-red"> <i class="fa fa-frown-o" aria-hidden="true"></i></h1>-->

    <div class="error-content">
        <h3><i class="fa fa-warning text-red"></i> Oops!</h3>
        <h5><b><?php echo $message; ?></b></h5>
<!--        <h5>Meanwhile, you may <a href="--><?php //echo base_url(); ?><!--">return to dashboard</a> or try using the search form.</h5>-->

        <form class="search-form">
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search">

                <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            <!-- /.input-group -->
        </form>
    </div>
    <!-- /.error-content -->
</div>