<?php
/**
 * Created by PhpStorm.
 * User: Nirjhar
 * Date: 9/17/2017
 * Time: 4:52 PM
 */
header_js(['plugins/ckeditor/ckeditor.js']);
?>
<?php if(!empty($docs)){ ?>
<div class="col-md-2 no-padding" style="position: relative">
    <?php foreach ($docs as $doc){  ?>
        <p><a href="#<?php echo $doc->doc_id; ?>" class="doc_link"><i class="fa fa-hand-o-right"></i> <?php echo html_entity_decode($doc->doc_brief); ?></a><p>
    <?php } ?>
</div>
<div class="col-md-10" style="height: 800px; overflow-y:auto" id="docdiv">
    <?php foreach ($docs as $doc){ ?>
    <div class="box box-default box-solid" id="<?php echo $doc->doc_id; ?>">
        <div class="box-header with-border">
            <h3 class="box-title" data-widget="collapse" style="cursor: pointer;">
                <i class="fa fa-minus"></i>&nbsp;
                <?php echo html_entity_decode($doc->doc_brief); ?>
            </h3>
            <div class="box-tools pull-right">
                <a type="button" class="btn btn-box-tool" href="<?php echo base_url('home/documentation/edit/').$doc->doc_id; ?>">
                    <i class="fa fa-pencil"></i>
                </a>
            </div>
        </div>
        <div class="box-body">
            <?php echo html_entity_decode(htmlspecialchars($doc->doc_details)); ?>
        </div>
    </div>
    <?php } ?>
</div>
<script>
//    $.fn.extend({
//        scrollTo : function(speed, easing) {
//            return this.each(function() {
//                var targetOffset = $(this).offset().top;
//                $('html,body').animate({scrollTop: targetOffset}, speed, easing);
//            });
//        }
//    });
    jQuery.fn.scrollTo = function(elem, speed) {
        $(this).animate({
            scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top
        }, speed == undefined ? 300 : speed);
        return this;
    };
    $('.doc_link').on('click', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        $("#docdiv").scrollTo(link, 300);
//        $(''+link).scrollTo(400, 'linear');
    });
</script>
<?php }else{ ?>
    <div class="col-md-12">
        <div class="box-group" id="accordion">
            <div class="panel box box-danger">
                <div class="box-header with-border">
                    <h4 class="box-title"> Create DOC</h4>
                    <?php if(isset($edoc->doc_id) && !empty($edoc->doc_id)){ ?>
                    <div class="box-tools pull-right">
                        <a type="button" class="btn btn-sm btn-danger confirm-link" href="<?php echo base_url('home/documentation/delete/').$edoc->doc_id; ?>">
                            <i class="fa fa-close"></i> DELETE
                        </a>
                    </div>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <?php
                        $action_url = base_url('home/documentation/new');
                        if(isset($edoc->doc_id) && !empty($edoc->doc_id)){
                            $action_url = base_url('home/documentation/edit/').$edoc->doc_id;
                        }
                    ?>
                    <form action="<?php echo $action_url; ?>" method="post">
                        <div class="col-md-9 no-padding">
                            <textarea id="ckeditor" class="ckeditor" name="doc_details"><?php echo htmlspecialchars(@$edoc->doc_details); ?></textarea>
                        </div>
                        <div class="col-md-3">
                            <label>Doc Brief</label>
                            <textarea class="form-control" name="doc_brief" rows="10"><?php echo @$edoc->doc_brief; ?></textarea>
                            <br/>
                            <?php
                                $action_button = "CREATE";
                                if(isset($edoc->doc_id) && !empty($edoc->doc_id)){
                                    $action_button = "UPDATE";
                                }
                            ?>
                            <button type="submit" class="btn btn-primary btn-flat btn-block btn-lg"><?php echo $action_button; ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="row"></div>
