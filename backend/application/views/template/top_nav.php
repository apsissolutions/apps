<header class="main-header">
        <a href="<?php echo base_url('dashboard'); ?>" class="logo bg-blue-gradient">
            <span class="logo-mini"><b>IOI</b></span>
            <span class="logo-lg"><b>Oooops</b> !!</span>
        </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top bg-blue-gradient">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!--                 Module Name Here-->
                <li class="dropdown notifications-menu">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="<?php echo $module_name->module_icon ?>"></i>
                        <?php echo $module_name->module_name; ?>
                        <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu"><?php echo $module_html ?></ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url().''.$this->session->userdata('USER_IMAGE'); ?>" class="user-image" alt="<?php echo $this->session->userdata('FIRSTNAME'); ?>">
                        <span class="hidden-xs"><?php echo $this->session->userdata('FIRSTNAME'); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url().''.$this->session->userdata('USER_IMAGE'); ?>" class="img-circle" alt="<?php echo $this->session->userdata('FIRSTNAME'); ?>">
                            <p>
                                <?php echo $this->session->userdata('FIRSTNAME'); ?> <?php echo $this->session->userdata('LASTNAME'); ?>
                                <!-- <small>Member since Nov. 2012</small>-->
                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url('master/edit_profile')?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('login_cont/logout')?>" class="btn btn-danger btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>