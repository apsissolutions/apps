<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Custom_Controller {
    function __construct() {
        parent::__construct();
    }
/*-----------------------------------------------------------------------------------------*/
    public function index() {
        $level = $this->session->userdata('LEVEL_ID');
        if(!isset($level) && empty($level)){
            redirect(base_url('login_cont'));
        }else{
             $this->render_page('index');
        }
    }
    public function documentation($create = '', $id = ''){
        if(empty($create)){
            $data['docs'] = $this->db->query("SELECT * FROM doc")->result();
        }else{
            $post = $this->input->post();
            if(!empty($id) && $create == 'delete'){
                $data['edoc'] = $this->db->query("DELETE FROM doc WHERE doc_id = $id");
                $this->setFlashData('danger', 'Documentation item has been deleted !!');
                redirect('home/documentation');
            }
            if(!empty($id)){
                $data['edoc'] = $this->db->query("SELECT * FROM doc WHERE doc_id = $id")->row();
            }
            if(!empty($post)){
                $doc_data = array(
                    'doc_brief'=>$post['doc_brief'],
                    'doc_details'=>$post['doc_details']
                );
//                dd($post);
                if(!empty($id)){
                    $this->db->WHERE('doc_id', $id);
                    $this->db->UPDATE('doc', $doc_data);
                }else{
                    $this->db->INSERT('doc', $doc_data);
                }
                $this->setFlashData('success', 'Operation Successfull!!');
                redirect('home/documentation');
            }
            $data['docs'] = array();
        }
        $this->render_page('documentation', $data);
    }
    public function err_404(){
        $data['message'] = "Cant exist the url you where trying to reach.";
        $menu_html_array = $this->get_menu();
        $menu_html['menu_list'] = $menu_html_array['menu'];
        $menu_html['new_submenu_list'] = $this->parent_menu_id ? $this->display_children($this->parent_menu_id):'';
        $menu_html['d_menu'] = $menu_html_array['all_menu'];
        $menu_html['url_data'] = $this->parent_menu_id;
        $menu_html['module_html'] = $this->get_module_list();
        $menu_html['module_name'] = $this->get_active_module();
        $menu_html['menu_array'] = $this->menu_array;
        $this->load->view('template/header', $menu_html);
        $this->load->view('template/top_nav', $menu_html);
        $this->load->view('template/menu', $menu_html);
        $this->load->view('errors/html/error_404', $data);
        $this->load->view('template/footer', $data);
    }
}