<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Master extends Custom_Controller {
    public $all_child_array;
    function __construct() {
        parent::__construct();
        $this->load->model('master_model','','TRUE');
    }
    public function index(){
        if(is_post()){
            $data['selected_title'] = $this->input->post('master_entry_title', TRUE);
            $data['selected_table'] = $this->master_model->get_original_table_name($data['selected_title']);
//            dd($data['selected_table']);
            $data['record'] = $this->master_model->get_table($data['selected_table']);
            $master_entry_table_name = $data['selected_table'];
            $record_previous = $this->master_model->get_previous_table($master_entry_table_name, $data['selected_title']);
            $check_data = array();
            $extra_data = array();
            $extra_table = array();
            if(!empty($record_previous)){
                foreach ($record_previous as $r_p){
                    $check_data[$r_p['field_name']] = $r_p;
                }
                $get_data = $this->master_model->get_extra_relation_with_table($data['selected_table'],$data['selected_title']);
                //print_r($get_data);
                $counter = 0;
                foreach ($get_data as $value){
                    $counter++;
                    $extra_data[] = array(
                        0=>'<tr><td><input type="checkbox" name="count_hidden[]" value="'.$counter.'" class="count_hidden" checked /></td>
                                <td>',
                        1=>$value['join_with'],
                        2=>'</td><td id="table_field_'.$counter.'">',
                        3=>'</td><td>',
                        4=>$value['field_name'],
                        5=>'</td>
                                <td><textarea class="form-control" name="sql_extra['.$counter.']" placeholder="SQL query">'.$value['sql'].'</textarea></td>
                                <td><input type="text" class="form-control" name="label_name_extra['.$counter.']" placeholder="Label Text" value="'.$value['label_name'].'" /></td>
                            </tr>',
                        6=>$value['join_with_fk_name']);
                }
            }
            $data['extra_data'] = $extra_data;
            $data['check_data'] = $check_data;

            // Get the list of master entry table name
            $data['master_table'] = $this->master_model->get_master_table();
            $search = $this->db->query("SELECT include_search_panel FROM master_entry_table WHERE master_entry_title='".$data['selected_title']."'")->row();
            if($search->include_search_panel == 1){
                $data['search'] = 'search';
            }
        }
        // Get duplicate master entry title
        $data['mask_table_name'] = $this->master_model->get_mask_table();

        $this->render_page('master/master_table', $data );
    }
    public function create_master(){
        $fields = $this->input->post('fields');
        $lab = $this->input->post('label');
        $place = $this->input->post('placeholder');
        $master_entry_table_name = $this->input->post('module');
        $master_entry_title = $this->input->post('master_entry_title');
        $sql = $this->input->post('sql');
        $hide_from_grid = $this->input->post('hide_from_grid');
        $self_filtering = $this->input->post('self_filtering');
        $hide_from_input = $this->input->post('hide_from_input');
        $comma_separated_value = $this->input->post('comma_separated_value');
        $input_type = $this->input->post('input_type');
        $required_type = $this->input->post('required');
        $search = $this->input->post('search');
        $request_for = $this->input->post('show')?'show':'create';

        /*
         * table_content() function perform the insertion the details of requested table for creating master entry
         * And return all value into array from master_entry taable to create view of the requested table
         */
        
        $data_master_form = $this->master_model->table_content($fields,$lab,$place,$sql,$hide_from_grid,$self_filtering,$hide_from_input,$comma_separated_value,$input_type,$required_type,$master_entry_table_name,$request_for,$master_entry_title,$search);
        
        //Get the data for additional table to join
        if(!null == ($this->input->post('count_hidden'))){
        $join_count = $this->input->post('count_hidden');
        $join_table_name = $this->input->post('master_entry_table_name');
        $join_field_name = $this->input->post('master_entry_table_field_name');
        $join_relation_field_name = $this->input->post('relational_field');
        $join_sql = $this->input->post('sql_extra');
        $join_label = $this->input->post('label_name_extra');
        
        // Perform insertion to master_entry
        $this->master_model->join_insertion($join_count, $join_table_name,$join_field_name,$join_relation_field_name,$join_sql,$join_label,$master_entry_table_name,$master_entry_title);
        }
        
        $this->get_html($data_master_form, $master_entry_title);
    }
    public function show_master($mask_name = NULL, $msg = NULL){
        $p = $this->input->post();
        $builder = '';
        if(isset($p) && !empty($p)){
            foreach ($p as $key => $value) {
                if($value!='')$builder .=" AND ".$key."= '".$value."'";
            }
        }
        /* End of the newly added part */
        
        // If requested for any table that is not exist in the database server.
        if(!$mask_name){
            show_404 ();
        }else{
            $query = $this->db->query("SELECT * FROM master_entry WHERE master_entry_title = '$mask_name' ORDER BY sorting");
            $data_master_form = $query->result_array();
            $menu_id = $this->input->get('m_id');
            $this->get_html($data_master_form, $mask_name, NULL,$menu_id, $msg, $builder,$p);
        }
    }
    //Function to update the master entry
    public function update_content($view_name,$id){
        $data_master_form = $this->master_model->get_array_result("SELECT * FROM master_entry WHERE master_entry_title = '$view_name' ORDER BY sorting");
        $this->get_html($data_master_form, $view_name, $id, $this->input->get('m_id'));
    }
    public function delete_content($view_name,$id){
        // Check privilege for update master table content (Not applicable for Admin user_id=1)
       $table_name = $this->db->query("SELECT master_entry_table_name FROM master_entry_table WHERE master_entry_title = '$view_name'")->row()->master_entry_table_name;
       $msg = '';
       $get_menu_id = $this->db->query("SELECT menu_id FROM menu WHERE menu_url='master/show_master/".$view_name."'")->row()->menu_id;
        if( $this->user_id != 1 ){
            $this->db->select('GROUP_CONCAT(privilege_delete) AS level');
            $this->db->where_in('user_level_id',  $this->session->userdata('LEVEL_ID'));
            $get_permission = $this->db->get('user_level')->row()->level;
            $permission_array = explode(',', $get_permission);
            // If found any edit permission among all of his level
            if(!in_array('1', $permission_array)){
                $msg = $this->permission_deny;
                $this->show_master($view_name, $msg);
            }
            else{
                $id_field = $table_name.'_id';
                $this->db->delete($table_name, array($id_field => $id));
                //$this->show_master($view_name);
                redirect(base_url().'master/show_master/'.$view_name.'?m_id='.$get_menu_id);
            }
        }else{
            $id_field = $table_name.'_id';
            $ret = $this->db->delete($table_name, array($id_field => $id));

            //$this->show_master($view_name);
            redirect(base_url().'master/show_master/'.$view_name.'?m_id='.$get_menu_id);
        }  
    }
    public function get_html($data_master_form, $master_entry_table_title, $id = NULL, $menu_id=null, $msg = NULL, $builder = NULL, $post_value = NULL){
        //print_r($data_master_form);
        // Now get the original table name
        $master_entry_table_name = $this->master_model->get_original_table_name($master_entry_table_title);
        $hdn_arr = array();
        $unique_exception = '';
        if($id != NULL){
            $id_field = $master_entry_table_name.'_id';
            $get_data_from_table = $this->master_model->get_array_result("SELECT * FROM $master_entry_table_name WHERE $id_field='$id'");
            $data_to_pass['update_command'] = $id;
            $unique_exception = ' entry_id="'.$id.'" ';
            //print_r($get_data_from_table);
        }
        // Now read the query and where filter for requested table inorder to generate grid data
        $sql = "SELECT * FROM master_entry_table WHERE master_entry_title='$master_entry_table_title'";
        $get_sql_and_where_filter = $this->db->query($sql)->row();
        $form = array();
        $table_field = array();
        $label_list = array();
        $search_param = array();
        $search_html = '';
        foreach ($data_master_form as $rec2){
            if($get_sql_and_where_filter->disable_grid_view != 1 && $rec2['search'] == 1){
                //$search_param[$rec2['field_name']][] = $rec2['search'];
                //$search_html .= $rec2['field_name'].'<input type="" />';
            }
            if($rec2['hide_from_input'] !=1 ){
                $input='';
                $filter_class = $rec2['self_filtering'] == 1 ? ' class="search"':'';
                $field_name = $rec2['field_name'];
                $field_key = $rec2['field_key'];
                // When field is unique then add some validation content
                $unique = $field_key == 'UNI'?' field_type="unique" data-table_name="'.$master_entry_table_name.'"':'';
                $draw_message = $field_key == 'UNI'?'<div class="draw_msg"></div>':'';
                $label = $rec2['label_name'];       
                $type = $rec2['input_type'];
                $sql_str = $rec2['sql'];
                $comma_separated_val = $rec2['comma_separated_value'];
                $placeholder = $rec2['placeholder'];
                $validation = $rec2['required_field'];
                $required_str = ($validation == 'required')?'<span style="color:#f00"><i class="fa fa-star"></i></span>':'';
                $label_name = "<div class='form-group'>";
                $label_name .= $required_str;
                $label_name .= ' <label class="labeltochange" data-msentry="'.$master_entry_table_title.'" id="'.$field_name.'">'.strtoupper($label).'</label>';
                $label_name .= "<br/>";
                $slabel_name = "<div class='form-group'><label>$label</label>";
                $is_join = $rec2['join_with'];
                // Here it will check the join field and submitted id to retrive value from related joined table
                //echo $type.' -- ';
                if( $is_join != NULL && $id !=NULL ){
                    $selected_value = array();
                    $sql = "SELECT $field_name FROM $is_join WHERE ".$rec2['join_with_fk_name']."=$id";
                    $query = $this->db->query($sql);
                    $data = $query->result_array();
                    foreach ($data as $value) {
                        $selected_value[] = $value[$field_name];
                    }
                    //print_r($data);
                }else{
                     $selected_value = isset($get_data_from_table)?$get_data_from_table[0][$field_name]:'';
                     $selected_value_search = ($post_value != NULL && isset($post_value[$field_name])) ? $post_value[$field_name]:'';
                }
//                dd($comma_separated_val.$type);
                switch ($type){
                    case 'select':
                        $options = $is_join==NULL?array(''=>'Select'):array();
                        $input = $label_name;
                        if($sql_str!=''){
                            $res = $this->master_model->array_result($sql_str);
                            foreach ($res[1] as $value) {
                                $options[$value[$res[0][0]]] = $value[$res[0][1]];
                            }
                        }else{
                            $opt = explode(',', $comma_separated_val);
                            foreach ($opt as $value) {
                                $options[$value] = $value;
                            }
                        }
                        $input .= form_dropdown($field_name.($is_join==NULL?"":"[]"), $options,$selected_value,'class="form-control multi"'.$validation.($is_join==NULL?"":" multiple")).'</div>';
                        $search_html .= ($rec2['search'] == 1?$slabel_name.form_dropdown($field_name.($is_join==NULL?"":"[]"), $options,$selected_value_search,'class="form-control"'.($is_join==NULL?"":" multiple")).'</div>':'');
                        break;
                    case 'var':
                        $input = "$label_name<input $validation $unique $unique_exception type='text' name='$field_name' placeholder='$placeholder' value='$selected_value' class='form-control'/>$draw_message</div>";
                        $selected_value_search = $selected_value_search == '' ? $comma_separated_val : $selected_value_search;
                        $search_html .= ($rec2['search'] == 1?"$slabel_name<input type='text' name='$field_name' placeholder='$placeholder' value='$selected_value_search' class='form-control'/></div>":'');
                        break;
                    case 'int':
                        if($field_key == 'MUL'){
                            $foreign_table = substr($rec2['field_name'], 0, -3);
                            $foreign_id = $foreign_table.'_id';
                            $foreign_name = $foreign_table.'_name';
                            $foreing_info = $this->master_model->combo_table($foreign_table); //model call
                            $input .= "$label_name<select class='form-control' name='$field_name'>";
                            $input .= "<option value=''>Select</option>";
                            foreach ($foreing_info as $foreing_info1){
                                $input .= "<option value='$foreing_info1[$foreign_id]'>$foreing_info1[$foreign_name]</option>";
                            }
                            $input .= "</select></div>";
                            if($rec2['search'] == 1){
                                $search_html .= "$slabel_name<select class='form-control' name='$field_name'>";
                                $search_html .= "<option value=''>Select</option>";
                                foreach ($foreing_info as $foreing_info1){
                                    $search_html .= "<option value='$foreing_info1[$foreign_id]'>$foreing_info1[$foreign_name]</option>";
                                }
                                $search_html .= "</select></div>";
                            }
                        }
                        break;
                    case 'dat':
                        $input = "$label_name<input $validation type='text' name='$field_name' placeholder='$placeholder' value='$selected_value' class='form-control datetimepicker'/></div>";
                        break;
                    case 'tex':
                        $input = "$label_name<textarea $validation name='$field_name' class='form-control'>$selected_value</textarea></div>";
                        $selected_value_search = !isset($selected_value_search) && empty($selected_value_search) ? $comma_separated_val : $selected_value_search;
                        $search_html .= ($rec2['search'] == 1?"$slabel_name<input type='text' name='$field_name' placeholder='$placeholder' value='$selected_value_search' class='form-control'/></div>":'');
                        break;
                    case 'hidden':        
                        $input = "<div class='form-group'><input type='$type' name='$field_name' placeholder='$placeholder' value='".$this->session->userdata($field_name)."' class='form-control'/></div>";
                        break;
                    case 'textarea':        
                        $input = "$label_name<textarea $validation name='$field_name' class='form-control'>$selected_value</textarea></div>";
                        $search_html .= ($rec2['search'] == 1?"$slabel_name<textarea $validation name='$field_name' class='form-control'>$selected_value_search</textarea></div>":'');
                        break;
                    case 'checkbox':
                        $input = "$label_name <input $validation type='hidden' name='$field_name' placeholder='$placeholder' value='0' />"
                                            . "<input $validation type='$type' name='$field_name' placeholder='$placeholder' value='1' ".($selected_value == 1?'checked':'')."/></div>";
                        break;
                    default :
                        $input = "$label_name<input step='any' $unique $unique_exception  $validation type='$type' name='$field_name' placeholder='$placeholder' value='$selected_value' class='form-control'/>$draw_message</div>";
                        $search_html .= ($rec2['search'] == 1 ? "$slabel_name<input type='text' name='$field_name' placeholder='$placeholder' value='$selected_value_search' class='form-control'/>":'');
                        break;
                }

                if( $is_join != NULL){
                    $hdn_arr[$is_join] = $field_name;
                }
                // When form view is not disable need to draw the form otherwise not
                if($get_sql_and_where_filter->disable_form_view == 1) {
                    $input = '';
                }else {
                    $form[] = $input;
                }
            }
            //When hide_from_grid is true then add lebel and field into array for generating view of master entry 
            //Otherwise the field will not show in the table view of master entry
            if($rec2['hide_from_grid'] != 1){
                if($get_sql_and_where_filter->disable_grid_view != 1){
                    $label_list[] = "<th $filter_class>".str_replace(' ', '_', $rec2['label_name'])."</th>";
                    $table_field[] = $rec2['field_name'];
                }
            }   
        }
        $get_sql = $get_sql_and_where_filter->table_sql;
        $filter = $get_sql_and_where_filter->where_clause_filter;
        $export_pdf = $get_sql_and_where_filter->export_pdf;
        $export_excel = $get_sql_and_where_filter->export_excel;
        $export_csv = $get_sql_and_where_filter->export_csv;
        $enable_printing = $get_sql_and_where_filter->enable_printing;
        $import_excel = $get_sql_and_where_filter->export_excel;
        $form_column = $get_sql_and_where_filter->form_column;
        $action_column = $get_sql_and_where_filter->disable_action;
        $disable_modal = $get_sql_and_where_filter->disable_modal;
        $edit_icon = $get_sql_and_where_filter->disable_edit_icon;
        $delete_icon = $get_sql_and_where_filter->disable_delete_icon;
        //$get_sql = str_replace ('WHERE 1', "", $get_sql);
        $full_sql = explode("GROUP BY", $get_sql);
        $get_sql = $full_sql[0];
        $remain_part = @$full_sql[1];
        if (strpos($get_sql,'WHERE') == false) {
            $get_sql .= " WHERE 1 ";
        }
        $view_all_privilege = $this->check_privilege($menu_id,3);
        if(!$view_all_privilege){
            $get_sql = str_replace ('WHERE 1', "WHERE created_by='".$this->user_id."'", $get_sql);
        }
        //$get_sql .= " AND $master_entry_table_name.view_name='$master_entry_table_title'";
        if($filter != '' && $filter != NULL && $this->user_id != 1){
            $arr = explode(',',$filter);
            foreach($arr as $val){
                $val = explode(':', $val);
                $get_sql .= " AND $val[0]='". $this->session->userdata($val[1]) ."'";
            }
        }
        if($builder != NULL)$get_sql = $get_sql.$builder;
        $remain_part = $remain_part == '' ? $remain_part : " GROUP BY ".$remain_part;
        $get_sql = $get_sql.$remain_part;
        $table_data = $this->master_model->array_result($get_sql);
        
        //$data_to_pass['post_value'] = $post_value;
        $data_to_pass['hdn_array'] = $hdn_arr;
        $data_to_pass['input_html'] = $form;
//        dd($form, true);
        $data_to_pass['table_data'] = $table_data;
        $data_to_pass['label_list'] = $label_list;
        $data_to_pass['field_name'] = $table_field;
        $data_to_pass['table_name'] = $master_entry_table_name;
        $data_to_pass['view_name'] = $master_entry_table_title;
        $data_to_pass['sorting_privilege'] = 0;
        // If admin or user of current module then get the sorting privilege
        if( $this->user_id == 1 || in_array(1, $this->session->userdata('MODULE_ID'))){
            $data_to_pass['sorting_privilege'] = 1;
        }
        if( $msg != NULL){
            $data_to_pass['msg'] = $msg;
        }
        //$menu_id = $menu==NULL?$this->
        // Now get the permission for doing things i.e. Add/Edit/Delete/View All
        $data_to_pass['edit_privilege'] = $this->check_privilege($menu_id,0);
        $data_to_pass['delete_privilege'] = $this->check_privilege($menu_id,1);
        $data_to_pass['add_privilege'] = $this->check_privilege($menu_id,2);
        $data_to_pass['enable_details_view'] = $this->check_enable_view($master_entry_table_name);
        //echo '<pre>';
        //print_r($data_to_pass);
        //exit;
        $data_to_pass['disable_action_column'] = $action_column;
        $data_to_pass['disable_edit_icon'] = $edit_icon;
        $data_to_pass['disable_delete_icon'] = $delete_icon;
        $data_to_pass['form_column'] = $form_column;
        $data_to_pass['search_panel'] = $search_html;
        $data_to_pass['disable_modal'] = $disable_modal;
        $data_to_pass['export_pdf'] = $export_pdf;
        $data_to_pass['export_excel'] = $export_excel;
        $data_to_pass['export_csv'] = $export_csv;
        $data_to_pass['import_excel'] = $import_excel;
        $data_to_pass['enable_printing'] = $enable_printing;
        $data_to_pass['menu_id'] = $this->input->get('m_id');
//        dd($data_to_pass);
        $this->render_page('master/master_form', $data_to_pass);
    }

    public function LabelChange($mode = 'master', $master_entry_name = ''){
        $name = $this->input->post('name');
        $value = $this->input->post('value');
        if(!empty($mode) && $mode == 'master'){
            $label = strtoupper(str_replace('_', ' ', $name));
            if(!empty($master_entry_name)){
                $sql = "UPDATE master_entry SET label_name = '$value' WHERE master_entry_table_name = '$master_entry_name' AND field_name = '$name'";
                $this->db->query($sql);
            }else{
                echo $label;
            }
        }else{
            $label_field = $this->language.'_label';
            $this->db->query("INSERT INTO label_slug SET lang_slug = '$name', $label_field = '$value' ON DUPLICATE KEY UPDATE $label_field = '$value'");
        }
    }
    public function form_entry(){
        $get_entry = $this->input->post();
        $table_name = $get_entry['table_name'];
        $view_name = $get_entry['view_name'];
        $id_field = $get_entry['table_name'].'_id';
        // Get the selected upload directory
        $directory = $this->master_model->get_upload_directory($view_name);
        
        $config['upload_path'] = $directory;
        $config['allowed_types'] = 'gif|jpg|png|pdf|xlsx|docx';
        foreach ($_FILES as $fieldname => $fileObject){
            if (!empty($fileObject['name'])){
                $this->upload->initialize($config);
                if (!$this->upload->do_upload($fieldname)){
                    $error = array('error' => $this->upload->display_errors());
                    $this->render_page('upload_form', $error);
                    //print_r($error);
                }else{
                     $reruned_data = $this->upload->data();
                     $get_entry[$fieldname] = $directory.$reruned_data['file_name'];
                }
            }
        }
        //Check wheather an ID is submitted for update other consider it as insert command
        $id = isset($get_entry[$get_entry['table_name'].'_id'])?$get_entry[$get_entry['table_name'].'_id']:NULL;
        
        //Remove unnecessary array item from post array
        unset($get_entry[$get_entry['table_name'].'_id']);
        unset($get_entry['table_name']);
        unset($get_entry['command']);
        unset($get_entry['view_name']);
        
        // We take $insert_val as array because we dont know the number table to join
        $insrt_val = array();
        $hdn_array = unserialize($get_entry['hdn_array']);
        // Now unset the hidden aarray from post
        unset($get_entry['hdn_array']);
        
        foreach ($hdn_array as $key => $value) {
            // Here key is the table name and $value is an array which contain post value and field name
            $insrt_val[$key] = array($get_entry[$value],$value);
            // Unset submitted value of join table from post array
            unset($get_entry[$value]);
        }
        
        // If no id submit then insert otherwise update
        if($id==NULL){
            $get_entry['created_by'] = $this->user_id;
            $get_entry['created'] = date('Y-m-d H:i:s');
            $this->db->insert($table_name, $get_entry);
            
            $id = $this->db->insert_id();
            
        }
        else{
            //$this->db->delete('privilege_level', array('user_id' => $user_id));
            $get_entry['updated_by'] = $this->user_id;
            //print_r($get_entry);
            $this->db->where($id_field, $id);
            $p = $this->db->update($table_name, $get_entry);
        }
        // Now fetch the value from the array which contain table name as $key
        foreach ($insrt_val as $key => $value) {
            $tbl_name = $key;
            $field_name = $value[1];
            $post_value = $value[0];
            // Now get the other relational field name form table
            $query = $this->db->query("SELECT * FROM master_entry WHERE master_entry_table_name='$table_name' AND field_name='$field_name' AND master_entry_title='$view_name'");
            $data_array = $query->result_array();
            //print_r($data_array);
            // Now get the field name of the parent table
            $relationa_field_name = $data_array[0]['join_with_fk_name'];
            
            // Now delete old data from joined table if exist
            $this->db->delete($tbl_name, array($relationa_field_name=>$id));
            
            // As we always use multiple dropdown it will always be an array where number of item can be one or more
            $data = array();
            foreach ($post_value as $item) {
                $data[] = array($relationa_field_name=>$id,$field_name=>$item);
            }
            //print_r($data);
            $this->db->insert_batch($tbl_name, $data);
        }
        $menu_id = $this->input->get('m_id');
        redirect(base_url().'master/show_master/'.$view_name.'?m_id='.$menu_id,'refresh');
        //$this->show_master($table_name);
    }
    /*=====================================================================================*/
    public function changeOrder($view_name = ''){
        $orderdata = $this->input->post('order');
        if(!empty($orderdata)){
            $this->master_model->reorder_entry_element($orderdata);
        }else{
            $this->db->select('label_name,field_name,master_entry_table_name');
            $this->db->where('master_entry_title', $view_name);
            $this->db->order_by('sorting');
            $query = $this->db->get('master_entry');
            $data['item_array'] = $query->result_array();
            $data['view_name'] = $view_name;
            $data['menu_id'] = $this->menu_id;
            $this->load->view('master/master_rearrange_element', $data);
        }
    }
    // This function is being called from ajax to change the sorting order of master entry element 
    public function reorder_element(){
        $data = $this->input->post('order');
        $this->master_model->reorder_entry_element($data);
    }
    //Create table for master entry with standard settings
    public function create_master_table(){
        if(is_post()){
            $table_name = $this->input->post('table_name');
            $this->master_model->create_master_table($table_name);
            $this->setFlashData('success', 'Table Created Successfully');
            redirect('master/create_master_table');
        }
        $this->render_page('master/create_master_table');
    }
    public function insert($table_name,$data,$url = NULL){
        $this->db->insert($table_name,$data);
        if($url!=NULL){
            redirect($url);
        }
    }
    public function delete($table_name,$id,$url = NULL){
        $this->db->delete($table_name, array($table_name.'_id' => $id)); 
        if($url!=NULL){
            redirect($url);
        }
    }
    public function update($table_name,$id,$data,$url = NULL){
        $this->db->where($table_name.'_id', $id);
        $this->db->update($table_name,$data); 
        if($url!=NULL){
            redirect($url);
        }
    }
    public function get_details_by_id($table_name,$id){
        $query = $this->db->get_where($table_name, array($table_name.'_id' => $id));
        return $query->result_array();
    }
    public function get_value_by_id($table_name,$id,$field_name){
        $this->db->select($field_name);
        $this->db->where($table_name.'_id', $id);
        $this->db->from($table_name);
        $query = $this->db->get();
        return $query->row()->$field_name;
    }

    //Function to apply level on specific user
    public function user_level_assign($user_id=NULL){
        $get_level = $this->master_model->get_permitted_level_list();
        if($user_id != NULL){
            $get_user_data = $this->master_model->get_permitted_level_list($user_id);
            $data['user_details'] = $get_user_data;
            $data['selected_user_id'] = $user_id;
        }
        $data['permitted_level'] = $get_level;
        $this->render_page('master/master_set_level_for_user',$data);
    }
    // Function to save applied level for user
    public function save_level_access_for_user(){
        $user_id = $this->input->post('user_id');
        $level_array = $this->input->post('user_level_id');
        //At first delete user data if exist in the privilege_level and then insert
        $this->db->delete('privilege_level', array('user_id' => $user_id));
        $data = array();
        foreach ($level_array as $level_id){
            $data[] = array('user_id'=>$user_id,'user_level_id'=>$level_id);
        }
        $this->db->insert_batch('privilege_level', $data);

        redirect(base_url().'master/user_level_assign');
    }

    public function get_table_field($table_name){
        $count = $this->input->post('count');
        table_field($table_name,'master_entry_table_field_name['.$count.']');
    }

    public function add_tr($count_val){
        $selected_table = $this->input->post('table_name');
        echo '<tr>
            <td><input type="checkbox" name="count_hidden[]" value="'.$count_val.'" class="count_hidden" checked /></td>
                  <td>';
        master_entry_table('master_entry_table_name['.$count_val.']',NULL,'',array('class'=>'extra_table','data-count'=>$count_val));
        echo'</td>
             <td id="table_field_'.$count_val.'"></td>
             <td>';
        table_field($selected_table,'relational_field['.$count_val.']');
        echo'</td>
            <td><textarea class="form-control" name="sql_extra['.$count_val.']" placeholder="SQL query"></textarea></td>
            <td><input type="text" class="form-control" name="label_name_extra['.$count_val.']" placeholder="Label Text" /></td>
        </tr>';
    }
    public function view_details($view_name = '', $id = ''){
        $url = 'dropdown_manager/dropdown/dropdown_sample/'.$id;
        modules::run($url);
    }
    public function generate_combo_by_ajax_call(){
        $function_name = $this->input->post('function_name');
        $field_name = $this->input->post('field_name');
        $id = $this->input->post('id');
        $value = $this->input->post('val');
        $onchange = $this->input->post('on_change')== 'TRUE'?TRUE:FALSE;
        $function_name(NULL,NULL,$field_name,array($id=>$value),$onchange);
    }

    public function send_email($from_mail,$from_name,$to,$subject,$message,$attachment = array(),$cc = '',$bcc = ''){
        $this->load->library('email');
        $this->email->from($from_mail, $from_name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->cc($cc); 
        $this->email->bcc($bcc);
        //$this->email->attach($attachment);
        if(!empty($attachment)){
            if(is_array($attachment)){
                foreach($attachment as $file_path){
                    $this->email->attach($file_path);
                }
            }else{
                $this->email->attach($attachment);
            }
        }
        $this->email->send();
    }
    /*============================= Privilege Checking Function ==============================================*/

    public function edit_profile(){
        $post = $this->input->post();
        if(empty($post)){
            $data['user_record'] = $this->master_model->user_information();
            $this->render_page('master/edit_user', $data);
        }
        else{
            if($this->input->post('user_submit')){
                extract($post);
                if(!empty($password)){
                    $data_info = array(
                        'username' => @$username,
                        'password' => md5(@$password),
                        'default_url' => @$default_url,
                        'secret_question_1' => @$secret_question_1,
                        'secret_question_2' => @$secret_question_2,
                        'secret_question_ans_1' => @$secret_question_ans_1,
                        'secret_question_ans_2' => @$secret_question_ans_2
                    );
                }
                else{
                    $data_info = array(
                        'username' => @$username,
                        'secret_question_1' => @$secret_question_1,
                        'secret_question_2' => @$secret_question_2,
                        'default_url' => @$default_url,
                        'secret_question_ans_1' => @$secret_question_ans_1,
                        'secret_question_ans_2' => @$secret_question_ans_2
                    );
                }
                $this->db->WHERE('user_id', $this->user_id);
                $this->db->UPDATE('user',$data_info);
                $data['user_record'] = $this->master_model->user_information($this->user_id);
                $this->render_page('master/edit_user', $data);
            }
            $first_name = '';
            $last_name = '';
            $date_of_birth = '';
            $email = '';
            $contact_number = '';
            $father_name = '';
            $mother_name = '';

            if($this->input->post('personal_submit')){
                extract($post);
                $data_info = array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'contact_number' => $contact_number,
                    'father_name' => $father_name,
                    'mother_name' => $mother_name
                );
                if($_FILES){
                    $config['upload_path']          = './images/users/';
                    $config['allowed_types']        = 'gif|jpg|png';
                    $config['overwrite']            = TRUE;
                    $config['file_name']            = $this->user_id;
                    $config['max_size']             = 300;
                    $file_info = $this->file_upload($config);
//                    dd($file_info);
                    $data_info['user_image'] = 'images/users/' . $file_info['orig_name'];
                    $sessdata['USER_IMAGE']= $data_info['user_image'];
                    $this->session->set_userdata($sessdata);
                }
                $this->db->WHERE('user_id', $this->user_id);
                $this->db->UPDATE('user', $data_info);
                $data['user_record'] = $this->master_model->user_information();
                $this->render_page('master/edit_user', $data);
            }
        } 
    }

    public function check_uniqueness(){
        $table_name = $this->input->post('table_name');
        $field_name = $this->input->post('field_name');
        $value = $this->input->post('field_value');
        $entry_id = $this->input->post('entry_id');
        $condition = $entry_id == 0? "":" AND ".$table_name."_id <> $entry_id";
        $count_duplicate = $this->db->query("SELECT count($field_name) AS duplicate FROM $table_name WHERE $field_name='$value' $condition")->row()->duplicate;
        if($count_duplicate > 0){
            $p = array('msg'=>'<div style="margin-top:3px;display:block;" class="label label-danger">Duplicate entry. Try another</div>','enable'=>'0');
            echo json_encode($p);
            //echo '<div style="margin-top:3px;display:block;" class="label label-danger">Duplicate entry. Try another</div>';
        }else{
            $p = array('msg'=>'<div style="margin-top:3px;display:block;" class="label label-success">I am safe.</div>','enable'=>'1');
            echo json_encode($p);
        }
    }
    public function import_excel($view_name = ''){
        if(is_post()){
            $file = $_FILES['file']['tmp_name'];
            //load the excel library
            $this->load->library('excel');
            //read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            $objPHPExcel->setActiveSheetIndex(0);
            //get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

            //extract to a PHP readable array format
            $number = 1;
            $arr_data = array();
            foreach ($cell_collection as $cell) {
                $index = 0;
                $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $colIndex = PHPExcel_Cell::columnIndexFromString($column);
                $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                
                //header will/should be in row 1 only. of course this can be modified to suit your need.
                if ($row == 1) {
                    $header[$number++] = $data_value;
                } else {
                    $arr_data[$row][$header[$colIndex]] = $data_value;
                }
            }
            //send the data in an array format
            $data['header'] = $header;
            $data['values'] = $arr_data;
            $this->db->insert_batch('nationality',$arr_data);
            print_r($data['values']);
        }else{
            $data['view_name'] = $view_name;
            $this->render_page('master/import_excel',$data);
        }
    }

    public function download_sample(){
        $view_name = $this->input->post('view_name');
        $get_table_field = $this->master_model->get_table_field_from_master_entry($view_name);
        //echo '<pre>';
        //print_r($get_table_field);
        //echo $this->get_letter(2);
        //exit;
        //https://degreedocumentation.wordpress.com/2012/08/17/how-to-format-a-number-cell-in-phpexcel/
        //
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle($view_name);
        //Now set title with field name
        $number = 0;
        foreach($get_table_field as $field){
            $this->excel->getActiveSheet()->setCellValue($this->get_letter($number++).'1', $field['field_name']);
            //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
            //$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $filename=$view_name.'_sample.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        
    }
    
    private function get_letter($numeric_value){
        $letter = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
        return $letter[$numeric_value];
    }


    /*----------------------------------------------------class end-------------------------------------------------*/
}
