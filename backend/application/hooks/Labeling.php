<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nirjhar
 * Date: 9/13/2017
 * Time: 6:59 PM
 */
class Labeling {
    function getLabel(){
        $CI =& get_instance();
        $lang = 'en';
        $option_value = $lang.'_label';
        $sql = "SELECT lang_slug, $option_value FROM label_slug";
        $labels = $CI->db->query($sql)->result();
        if(!empty($labels)){
            foreach ($labels as $label){
                define($label->lang_slug, $label->$option_value) OR define($label->lang_slug, $label->lang_slug);
            }
        }
    }

}