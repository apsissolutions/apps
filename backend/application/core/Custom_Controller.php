<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . "third_party/MX/Controller.php";
class Custom_Controller extends MX_Controller {
    protected $data = array();
    public $menu_array = array();
    function __construct() {
        parent::__construct();
//         $this->output->enable_profiler(TRUE);
        date_default_timezone_set("Asia/Dacca");
        $this->load->model('main_model');
        $this->currdate = date('Y-m-d');
        $this->currdatetime = date('Y-m-d h:i:s');
        $this->user_id = $this->session->userdata('USER_ID');
        $this->language = $this->session->userdata('LANGUAGE');
        $this->module_id = $this->session->userdata('default_module_id');
        $this->get_permission_array = $this->main_model->get_privilege($this->user_id);
        $this->controller = $this->router->fetch_class();
        $this->method = $this->router->fetch_method();
        $this->uri_string = explode($this->method, $this->uri->uri_string())[0].$this->method;
        $this->menu_id = $this->input->get('m_id');
        $this->menuReading();
    }
    public function menuReading(){
        $this->parent_menu_id = $this->menu_id;
        if(empty($this->menu_id)){
            $menu_info = $this->main_model->getMenuInfoFromURL($this->uri_string, $this->module_id);
            if(!empty($menu_info)){
                $this->menu_id = $menu_info->menu_id;
                if($menu_info->menu_type == 'Sub' && !empty($menu_info->parent_menu_id)){
                    $this->parent_menu_id = $menu_info->parent_menu_id;
                }
            }
        }else{
            $menu_info = $this->main_model->getMenuInfoFromID($this->menu_id, $this->module_id);
            if(!empty($menu_info)){
                $this->menu_id = $menu_info->menu_id;
            }
            $this->parent_menu_id = $this->main_model->get_menu_parent($this->menu_id);
            if(empty($this->parent_menu_id)){
                $this->parent_menu_id = $this->menu_id;
            }
        }
    }
    /**
     * @param string $data
     */
    public function setFlashData($theme = '', $message = ''){
        $flashdata['theme'] = $theme;
        $flashdata['message'] = $message;
        $this->session->set_flashdata('flash_message', $flashdata);
    }
    /**
     * for loading sample static view file (template)
     * @param $view
     * @param string $data
     */
    public function render_page($view, $data = '') {
        if($this->session->userdata('LEVEL_ID') != '' || $this->user_id == 1){
            $is_permitted_to_view_this_page = $this->check_page_privilege($this->menu_id);
            if($is_permitted_to_view_this_page){
                $menu_html_array = $this->get_menu();
                $menu_html['menu_list'] = $menu_html_array['menu'];
                $menu_html['new_submenu_list'] = $this->parent_menu_id ? $this->display_children($this->parent_menu_id):'';
                $menu_html['d_menu'] = $menu_html_array['all_menu'];
                $menu_html['url_data'] = $this->parent_menu_id;
                $menu_html['module_html'] = $this->get_module_list();
                $menu_html['module_name'] = $this->get_active_module();
                $menu_html['menu_array'] = $this->menu_array;
                $this->load->view('template/header', $menu_html);
                $this->load->view('template/top_nav', $menu_html);
                $this->load->view('template/menu', $menu_html);
                $this->load->view($view, $data);
                $this->load->view('template/footer', $data);

            }else{
                $data['message'] = "You don't have access to the url you where trying to reach.";
//                show_error($message_403 , 403 );
                $menu_html_array = $this->get_menu();
                $menu_html['menu_list'] = $menu_html_array['menu'];
                $menu_html['new_submenu_list'] = $this->parent_menu_id ? $this->display_children($this->parent_menu_id):'';
                $menu_html['d_menu'] = $menu_html_array['all_menu'];
                $menu_html['url_data'] = $this->parent_menu_id;
                $menu_html['module_html'] = $this->get_module_list();
                $menu_html['module_name'] = $this->get_active_module();
                $menu_html['menu_array'] = $this->menu_array;
                $this->load->view('template/header', $menu_html);
                $this->load->view('template/top_nav', $menu_html);
                $this->load->view('template/menu', $menu_html);
                $this->load->view('errors/html/error_404', $data);
                $this->load->view('template/footer', $data);
            }
        }else{
            $url = current_url();
            redirect('/login_cont?url='.urlencode($url));
        }
    }
    /**
     * @return array
     */
    public function get_menu(){
        $get_menu_info = $this->main_model->get_menu_model(); // call model for getting menu and sub menu
        $menus = array();
        $all_menu = array();
        foreach ($get_menu_info as $get_menu_info1){
            $menu = '';
            $menu_name = $get_menu_info1['menu_name'];
            $menu_url = $get_menu_info1['menu_url'];
            $icon_class = $get_menu_info1['icon_class'];
            $menu_id = $get_menu_info1['menu_id'];
            /*-----submenus print here----------*/
            if($this->display_children($menu_id) != ''){
                $main_menu_class = "treeview";
                $down_arrow ="<i class='fa fa-angle-left pull-right'></i>";
                $submenu_class = "treeview-menu";
                $submenu_attribute = "";
                $submenu_html = "<ul class='{$submenu_class}'>".$this->display_children($menu_id)."</ul>";
            }
            else{
                $main_menu_class = "treeview";
                $submenu_class = "";
                $submenu_attribute = "";
                $submenu_html = "";
                $down_arrow = "";
            }
            /*-----submenus print here----------*/
            $class_name = '';
            if($menu_id == $this->parent_menu_id){
                $class_name = 'active';
            }
            $menu .= "<li class='$main_menu_class  $class_name'>";
            $menu .= "<a $submenu_attribute href='".base_url().$menu_url."?m_id=".$menu_id."'>";
            $menu .= "<i class='".$icon_class."'></i>  <span>".$menu_name."</span>".$down_arrow."</a>";
            $menu .= $submenu_html;
            $menu .= "</li>";
            $menus[] = $menu;
        }
        $get_all_menu_info = $this->main_model->get_menu_model('All');
        foreach ($get_all_menu_info as $value) {
            $get_all_menu = '<option value="'.$value['menu_id'].'">'.$value['menu_name'].'</option>'.$this->create_menu($value['menu_id']);
            $all_menu[] = $get_all_menu;
        }
        return array('menu'=>$menus,'all_menu'=>$all_menu);
    }
    /**
     * @param $parent
     * @return string
     * ======  Get SubMenu List   =========
     */
    public function display_children($parent) {
        $result = $this->main_model->get_menu($parent);
        $p =  "";

        foreach($result as $item){
            $class_name = '';
            if($this->menu_id == $item['menu_id']){
                $class_name = 'active';
            }
            if ($item['Count'] > 0) {

                $p .= "<li class='$class_name'>";
                $p .= "<a class='$class_name trigger right-caret' data-toggle='' href='#'></i> " . $item['menu_name'] . "</a>";
//                $p .= "<i class='fa fa-angle-left pull-right'></i>";
                $p .= "<ul class='treeview-menu'>";
                $p .= $this->display_children($item['menu_id']);
                $p .= "</ul>";
                $p .="</li>";
            } elseif ($item['Count']==0) {
                $p .= "<li class='$class_name' ><a data-toggle='' href='" . base_url().$item['menu_url'] . "?m_id=".$item['menu_id']."'><i class='".$item['icon_class']."'></i> " . $item['menu_name'] . "</a></li>";
            } else;
        }
        return $p;
    }
    /**
     * @param $parent
     * @return string
     * =======  Function for creating menu combobox   ==========
     */
    public function create_menu($parent) {
        static $str = 0;
        $str ++;
        $result = $this->main_model->get_menu($parent);
        $p =  "";
        $option_str = '';
        for($i=0;$i<$str;$i++)
            $option_str .= '&nbsp;&nbsp; ';
        foreach($result as $item){
            if ($item['Count'] > 0) {
                $this->menu_array[$parent]['submenu'][$item['menu_id']] = array('menu_name'=>$item['menu_name'],'submenu'=>$this->create_menu($item['menu_id']));
                $p .= "<option value='".$item['menu_id']."'>$option_str&clubs; ".$item['menu_name']."</option>";
                $p .= $this->create_menu($item['menu_id']);
            } elseif ($item['Count']==0) {
                $this->menu_array[$parent]['submenu'][$item['menu_id']] = array('menu_name'=>$item['menu_name']);
                $p .= "<option value='".$item['menu_id']."'>$option_str&clubs; ".$item['menu_name']."</option>";
            }
        }
        //$p .= "</ul>";
        $str--;
        return $p;
    }
    /**
     * @return string
     * ========  Function for creating module item  ========
     */
    public function get_module_list(){
        if($this->user_id == 1){
            $query = $this->db->query("SELECT * FROM module");
        }
        else{
            $query = $this->db->query("SELECT
                    module.module_id as module_id,
                    module.module_name as module_name,
                    module.module_icon
                    FROM
                    privilege_module
                    INNER JOIN module ON privilege_module.user_module_id = module.module_id
                    WHERE
                    privilege_module.user_id = '$this->user_id'");
        }
        $module_html = '';
        foreach ($query->result_array() as $value) {
            $module_html.= "<li><a href='".  base_url()."module_switcher/change_module/".$value['module_id']."'><i class='".$value['module_icon']."'></i> ".$value['module_name']."</a></li>";
        }
        return $module_html;
    }
    /**
     * @return mixed
     */
    public function get_active_module(){
        $module_id = $this->session->userdata('default_module_id');
        $module_name = $this->db->query("SELECT module_name,module_icon FROM module WHERE module_id=$module_id")->row();
        return $module_name;
    }
    /**
     * @param $name
     * @param $table_name
     * @param $id_field
     * @param $value_field
     * @param string $where
     * @param string $class
     * @param string $selected_value
     * @param bool $required
     * @param bool $readonly
     * @param null $ajax_target
     * @param bool $multiple
     * @param bool $blank
     * @return string
     */
    public function dynamic_combo($name,$table_name,$id_field,$value_field,$where='',$class='',$selected_value = '',$required=False,$readonly=false,$ajax_target=null,$multiple=FALSE,$blank=FALSE){
        $sql = "SELECT $id_field,$value_field FROM $table_name $where";
        $query = $this->db->query($sql);
        $data  = array(''=>'--Select--');
        foreach($query->result_array() as $value){
            $data[$value[$id_field]] = $value[$value_field];
        }
        $combo = form_dropdown($name, $data, $selected_value,"class='$class' ".($required?'reqiured':'')."  ".($readonly?'disabled':'')." ".($multiple?'multiple':'')."");
        return $combo;
    }
    /**
     * @param $config
     * @return mixed
     */
    public function file_upload($config){
        $this->load->library('upload');
        foreach($_FILES as $key => $inf){
            $get_file = $key;
        }
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($get_file)){
            return $this->upload->display_errors();
        }
        else{
            return $this->upload->data();
        }
    }
    /**
     * @param $menu_id
     * @param $privilege_index
     * @return bool
     */
    public function check_privilege($menu_id, $privilege_index){
        if( $this->user_id == 1 ){
            return TRUE;
        }else{
            //Check wherether the manu id exist in the array otherwise no nedd to check privilege
            if(is_array($this->get_permission_array) && array_key_exists($menu_id, $this->get_permission_array)){
                $array_val = $this->get_permission_array;
                if( $array_val[$menu_id][$privilege_index] > 0 ){
                    return TRUE;
                }else{
                    return FALSE;
                }
            }else{
                return FALSE;
            }
        }
    }
    /**
     * @param $menu_id
     * @return bool
     */
    public function check_page_privilege($menu_id){
        if( $this->user_id == 1 ){
            return TRUE;
        }elseif ($menu_id == '') {
            return TRUE;
        }else{
            if(array_key_exists($menu_id, $this->get_permission_array)){
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }
    /**
     * @param $table_name
     * @return mixed
     * Detect and get whether a details view will be eanble for any master entry table
     */
    public function check_enable_view($table_name){
        $get_val = $this->db->query("SELECT enable_details_view,template_details_view FROM master_entry_table WHERE master_entry_table_name='$table_name'")->row();
        return $get_val;
    }
    /**
     * @param $from_mail
     * @param $from_name
     * @param $to
     * @param $subject
     * @param $message
     * @param array $attachment
     * @param string $cc
     * @param string $bcc
     */
    public function send_email($from_mail,$from_name,$to,$subject,$message,$attachment = array(),$cc = '',$bcc = ''){
        //$this->load->library('email');

        $this->email->from($from_mail, $from_name);
        $this->email->to($to);
        $this->email->cc($cc);
        $this->email->bcc($bcc);
        if(!empty($attachment)){
            if(is_array($attachment)){
                foreach($attachment as $file_path){
                    $this->email->attach($file_path);
                }
            }else{
                $this->email->attach($attachment);
            }
        }
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();
    }
    /**
     * @param array $sql
     * @param array $columns
     * @param array $search
     *
     * $sql containing main_sql,where,group_by
     * $columns sql field name
     * $search which field are searchable
     * @ In search array we need data as table_name.field_name
     *
     * @return json data
     */
    public function generateDatatables($sql = [], $columns=[], $search=[], $key = '', $action = ''){
        if(!empty($_REQUEST)){
            $requestData = $_REQUEST;
            $final_sql='';

            $main_sql = $sql['main_sql'];
            $group_by='';
            $order_by='';
            // set where condition
            if(isset($sql['main_sql']) && $sql['main_sql']!=''){
                $where =" WHERE {$sql['where']}";
            }else{
                $where =" WHERE 1=1";
            }
            if(isset($sql['group_by']) && $sql['group_by']!=''){
                $group_by =" GROUP BY {$sql['group_by']}";
            }
            if(isset($sql['order_by']) && $sql['order_by']!=''){
                $order_by =" ORDER BY {$sql['order_by']}";
            }
            $final_sql = $main_sql.$where.$group_by.$order_by;

            $query = $this->db->query($final_sql);
            $data = $query->result_array();
            $totalData = $query->num_rows();
            $totalFiltered = $totalData;

            if( !empty($requestData['search']['value']) ) {
                $first =0;
                foreach($search as $col){
                    if($first==0){
                        $where .= " AND {$col} LIKE '".$requestData['search']['value']."%' ";
                    }else{
                        $where .="OR {$col} LIKE '".$requestData['search']['value']."%' ";
                    }
                    $first++;
                }
                $final_sql = $main_sql.$where.$group_by;

                $query=$this->db->query($final_sql);
                $totalFiltered = $query->num_rows(); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

                $final_sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
                $query=$this->db->query($final_sql);; // again run query with limit
            } else {
                $order_by =" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";

                $final_sql = $main_sql.$where.$group_by.$order_by;
                $query=$this->db->query($final_sql);
            }
            $data = $query->result_array();
//            dd($data);
            $finalData =[];
            foreach($data as $val){
                $temp =[];
                foreach ($columns as $col){
                    $temp[] = $val[$col];
                }
                if(!empty($key)){
                    $temp[] = str_replace('__KEY__', $val[$key], $action);
                }
                $finalData[] = $temp;
            }
            $json_data = array(
                "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
                "recordsTotal"    => intval( $totalData ),  // total number of records
                "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
                "data"            => $finalData   // total data array
            );
            return $json_data;
        }
    }
    /**
     * @param string $excel_title
     * @param array $sub_header
     * @param array $heading
     * @param array $data_row
     */
    public function excel_generator($excel_title = 'Excell Sheet',$sub_header = array(), $heading = array(), $data_row = array()){
        //dd($data_row,true);
        $this->load->library('excel');
        //Create a new Object
        $PHPExcel = new PHPExcel();
        $PHPExcel->getActiveSheet()->setTitle($excel_title);
        $objsheet = $PHPExcel->getActiveSheet();
        // styling
        $style_bold = array(
            'font' => array('bold' => true
            )
        );

        $style_border = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array(
                        ' rgb' => '000000'
                    )
                )
            )
        );

        $style_fill_cell = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'FFBB00')
            )
        );

        $style_fill_cell2 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '2B2E6C')
            ),
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => 'FFFFFF'),
                'size'  => 11,
                'name'  => 'Verdana'
            )
        );

        $objsheet->mergeCells("C2:G2");
        $objsheet->setCellValue("C2",$excel_title);
        $objsheet->getStyle("C2")->applyFromArray($style_bold);
        $objsheet->getStyle("C2")->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objsheet->getStyle("C2")->applyFromArray($style_fill_cell2);

        $rowNumberH = 4;
        $colH = 'A';

        if(!empty($sub_header)){
            $i = 1;
            foreach ($sub_header as $head=>$val){
                $objsheet->getStyle($colH.$rowNumberH)->applyFromArray($style_bold);
                $objsheet->getStyle($colH.$rowNumberH)->applyFromArray($style_fill_cell);
                //$objsheet->mergeCells("{$colH}:G2");
                $objsheet->setCellValue($colH.$rowNumberH,$head);
                $colH++;
                $i++;
                //$objsheet->getColumnDimension($colH)->set‌​AutoSize(true);
                $objsheet->setCellValue($colH.$rowNumberH,$val);
                $objsheet->getStyle($colH.$rowNumberH)->applyFromArray($style_bold);
                $colH++;
                $i++;
                if($i%4==0){
                    $rowNumberH++;
                }
            }
        }
        $colH = 'A';
        $rowNumberH +=2;
        foreach($heading as $head){
            //$last_colString = PHPExcel_Cell::stringFromColumnIndex($col);
            $objsheet->setCellValue($colH.$rowNumberH,$head);
            $colH++;
        }
        $data_start = $rowNumberH;
        $rowNumberH++;

        $row = $rowNumberH;
        foreach($data_row as $data){
            $col = 0;
            foreach($data as $col_data){
                $objsheet->setCellValueByColumnAndRow($col, $row, $col_data);
                $col++;
            }
            $row++;
        }

        //Freeze pane
        $objsheet->freezePane('A2');
        $last_colString = PHPExcel_Cell::stringFromColumnIndex(--$col);
        //dd($last_colString,true);
        $row--;
        $objsheet->getStyle("A{$data_start}:{$last_colString}{$data_start}")->applyFromArray($style_bold);
        $objsheet->getStyle("A{$data_start}:{$last_colString}{$data_start}")->applyFromArray($style_fill_cell);
        $objsheet->getStyle("A{$data_start}:{$last_colString}{$row}")->applyFromArray($style_border);
        //$data_start++;
        $objsheet->getStyle("A{$data_start}:{$last_colString}{$row}")->getFont()->setSize(9);
        //$objsheet->getStyle("A{$row}: {$last_colString}"  . --$row)->applyFromArray($style_border);
        //$objsheet->getStyle("A{$row}: {$last_colString}" . --$row)->getFont()->setSize(9);
        //Save as an Excel BIFF (xls) file
        $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel,'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $excel_title . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

}