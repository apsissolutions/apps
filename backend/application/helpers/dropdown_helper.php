<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @param $name
 * @param $sql
 * @param $where
 * @param $selected_value
 * @param $extra
 * @param $id_field
 * @param $value_field
 * @param bool $onchange
 * @return string
 * Function that will used by all Drop-Down Box creation method
 */
function common_in_combo($name,$sql,$where,$selected_value,$extra,$id_field,$value_field,$onchange = false){
    $CI =& get_instance();
    $condtion = '';
    if($where != NULL){
        $condtion = " WHERE 1";
        foreach ($where as $key => $value) {
            $condtion .= " AND $key='$value'";
        }
    }
    $sql = $sql.$condtion;
    $query = $CI->db->query($sql);
    $data = is_array($extra) && array_key_exists('multiple', $extra)?array() : array(''=>'Select');
    $extra_str='';
    if(!empty($extra)){
        foreach ($extra as $key => $value) {
            $extra_str .= $key.'="'.$value.'"';
        }
    }
    if($onchange){
        $extra_str = $extra_str.$onchange;
    }
    foreach ($query->result_array() as $value){
        $data[$value[$id_field]] = $value[$value_field];
    }
    return form_dropdown($name,$data,$selected_value,$extra_str);
}

if(!function_exists('__combo')){
    function __combo($slug = '', $data = array()){
        extract($data);
        $selected_value = isset($selected_value) && !empty($selected_value) ? $slug : '';
        $name = isset($name) && !empty($name) ? $name : '';
        $attributes = isset($attributes) && !empty($attributes) ? $attributes : [];
        $sql_query = isset($sql_query) && !empty($sql_query) ? $sql_query : '';
        if(!empty($slug)){
            $CI =& get_instance();
            $combodata = $CI->db->query("SELECT * FROM `dropdown` WHERE dropdown_slug = '$slug'")->row();
            if(!empty($combodata)){
                $sql = $sql_query == '' ? $combodata->sqltext : $sql_query;
                $query = $CI->db->query($sql)->result_array();
                $option_data = array();
                $attr = '';
                $attributes = empty($attributes) ? array('class'=>'form-control multi') : $attributes;
                if(!empty($attributes)){
                    foreach ($attributes as $key => $value) {
                        $attr .= $key.'="'.$value.'" ';
                    }
                }
                $multiple = isset($multiple) ? $multiple : $combodata->multiple;
                if($multiple == '1'){
                    $attr .= 'multiple = "true"';
                }else{
                    $attr .= '';
                    $option_data[''] = "Choose Option";
                }
                if(empty($name)){
                    $name = $combodata->dropdown_name;
                }
                foreach ($query as $value){
                    $value_field = $combodata->value_field;
                    $option_field = $combodata->option_field;
                    $option_data[$value[$value_field]] = $value[$option_field];
                }
                return form_dropdown($name, $option_data, $selected_value, $attr);
            }
        }
    }
}

/**
 * @param null $selected_value
 * @param array $extra_attr
 * @param string $name
 * @param null $where
 * Combobox for "user_level" table
 */
function user_level( $selected_value=NULL, $extra_attr=array('class'=>"form-control"), $name='user_level_id', $where=NULL ){
    $sql = "SELECT user_level_id, user_level_name FROM user_level";
    $id_field = 'user_level_id';
    $value_field = 'user_level_name';
    echo common_in_combo($name,$sql,$where,$selected_value,$extra_attr,$id_field,$value_field);
}

/**
 * @param null $selected_value
 * @param array $extra_attr
 * @param string $name
 * @param null $where
 * Combobox for "user" table
 */
function user( $selected_value = NULL, $extra_attr=array('class'=>"form-control"), $name='user_id', $where=NULL){
    $sql = "SELECT user_id,CONCAT(username,' > ',first_name,' ',last_name) as user_name FROM user";
    $id_field = 'user_id';
    $value_field = 'user_name';
    echo common_in_combo($name,$sql,$where,$selected_value,$extra_attr,$id_field,$value_field);
}

/**
 * @param null $selected_value
 * @param array $extra_attr
 * @param string $name
 * @param null $where
 * Combobox for "module" table
 */
function module( $selected_value=NULL, $extra_attr=array('class'=>"form-control"), $name='module_id', $where=NULL ){
    $sql = "SELECT module_id,module_name FROM module WHERE module_id<>1";
    $id_field = 'module_id';
    $value_field = 'module_name';
    echo common_in_combo($name,$sql,$where,$selected_value,$extra_attr,$id_field,$value_field);
}

/**
 * @param string $name
 * @param null $where
 * @param null $selected_value
 * @param array $extra_attr
 * Combobox for "master_entry" table
 */
function master_entry_table($name='master_entry_table_name',$where=NULL,$selected_value=NULL,$extra_attr=array('class'=>"form-control")){
    $sql = "SELECT master_entry_table_id,master_entry_table_name FROM master_entry_table";
    $id_field = 'master_entry_table_name';
    $value_field = 'master_entry_table_name';
    echo common_in_combo($name,$sql,$where,$selected_value,$extra_attr,$id_field,$value_field);
}

/**
 * @param string $name
 * @param null $where
 * @param null $selected_value
 * @param array $extra_attr
 * Combobox for "view" table
 */
function view_table($name='master_entry_table_name',$where=NULL,$selected_value=NULL,$extra_attr=array('class'=>"form-control")){
    $sql = "SELECT master_entry_table_id,master_entry_table_name FROM master_entry_table";
    $id_field = 'master_entry_table_name';
    $value_field = 'master_entry_table_name';
    echo common_in_combo($name,$sql,$where,$selected_value,$extra_attr,$id_field,$value_field);
}

/**
 * @param $table_name
 * @param string $name
 * @param null $where
 * @param null $selected_value
 * @param array $extra_attr
 * Combobox for get table field of a table
 */
function table_field($table_name,$name='master_entry_table_name',$where=NULL,$selected_value=NULL,$extra_attr=array('class'=>"form-control")){
    $sql = "DESCRIBE $table_name";
    $id_field = 'Field';
    $value_field = 'Field';
    echo common_in_combo($name,$sql,$where,$selected_value,$extra_attr,$id_field,$value_field);
}