<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @param string $level_id
 * @return bool
 * CHECK THE USER LEVEL EXIST IN CURRENT SESSION
 */
function __isLevel($level_id = ''){
    $CI =& get_instance();
    $user_level = $CI->session->userdata('LEVEL_ID');
//    $user_level = 1;
    if(in_array(1, $user_level)){
        return true;
    }else{
        if(!empty($level_id) && !empty($user_level)){
            if(in_array($level_id, $user_level)){
                return TRUE;
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
}

/**
 * for debugging
 * @param null $data
 * @param bool $die
 */
function dd($data = null, $die = false){
    if (is_string($data)) {
        print "<pre>";
        if($data == 'sess'){
            $CI =& get_instance();
            print_r($CI->session->all_userdata());
        }else{
            print $data;
        }
        print "</pre>";
    } else {
        print "<pre>";
        print_r($data);
        print "</pre>";
    }
    print "<hr />";
    if ($die) {
        exit();
    }
}
/**
 * @param null $name
 * @param null $title
 * @param null $attribute
 */
function file_browse($name = null, $title = null, $attribute = null){
    if($name == null){
        $name = 'image_up';
    }
    if($title == null){
        $title = 'Browse to Upload';
    }
    echo '<script>$(document).ready(function(){$(".file-inputs").bootstrapFileInput();});</script>';
    echo "<input class='file-inputs' type='file' name='".$name."' data-filename-placement='inside' title='".$title."' ".$attribute."/>";
}
/**
 * @param $date
 * @return bool|string
 * CONVERT TO BRITISH FORMAT DATE
 */
function toDated($date) {
    if (!empty($date)) {
        return date("j M, Y", strtotime($date));
    } else {
        return '';
    }
}

/**
 * @param $date
 * @return bool|string
 * CONVERT TO BRITISH FORMAT DATE-TIME
 */
function toDateTimed($date) {
    return date("j M, Y h:i A", strtotime($date));
}

/**
 * @param string $monthnum
 * @return string as Month Name
 */
function getMonthName($monthnum = ''){
    if(!empty($monthnum)){
        $dateObj = DateTime::createFromFormat('!m', $monthnum);
        return $dateObj->format('F');
    }else{
        return '';
    }
}

/**
 * @return string
 */
function getmenu(){
    $CI =& get_instance();
    $get = $CI->input->get('m_id');
    return isset($get) && !empty($get) ? '?m_id='.$get : '';
}

if(!function_exists('__result')){
    /**
     * @param string $sql
     * @return bool or query result
     */
    function __result($sql = ''){
        if(!empty($sql)){
            $CI =& get_instance();
            return $CI->db->query($sql)->result();
        }else{
            return false;
        }
    }
}
if(!function_exists('__row')){
    /**
     * @param string $sql
     * @return bool or query result
     */
    function __row($sql = ''){
        if(!empty($sql)){
            $CI =& get_instance();
            return $CI->db->query($sql)->row();
        }else{
            return false;
        }
    }
}
if(!function_exists('__result_array')){
    /**
     * @param string $sql
     * @return bool or query result
     */
    function __result_array($sql = ''){
        if(!empty($sql)){
            $CI =& get_instance();
            return $CI->db->query($sql)->result_array();
        }else{
            return false;
        }
    }
}
if(!function_exists('__affected')){
    /**
     * @return bool
     */
    function __affected(){
        $CI =& get_instance();
        return $CI->db->affected_rows();
    }
}
if(!function_exists('__query')){
    /**
     * @return bool
     */
    function __query($sql){
        $CI =& get_instance();
        return $CI->db->query($sql);
    }
}
if(!function_exists('is_post')){
    function is_post() {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
}
if(!function_exists('hasValue')){
    function hasValue($label_slug = ''){
        $CI =& get_instance();
        $sql = "SELECT COUNT(lang_slug) as haslabel FROM label_slug WHERE lang_slug = '$label_slug'";
        $haslabel = __row($sql)->haslabel;
        if($haslabel == 0) return false;
        else return true;
    }
}