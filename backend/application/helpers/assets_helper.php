<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: nijhu
 * Date: 04-Mar-17
 * Time: 10:31 PM
 */

/*
 * Dynamically add Javascript files to header page
 */
if(!function_exists('header_js')){
    function header_js($file=''){
        loaderCssJs('header_js', $file);
    }
}
/*
 * Dynamically add CSS files to header page
 */
if(!function_exists('header_css')){
    function header_css($file=''){
        loaderCssJs('header_css', $file);
    }
}
/*
 * Dynamically call the js/css loader function for header on individual page
 */
if(!function_exists('loaderCssJs')){
    function loaderCssJs($function_name, $file){
        $str = '';
        $item = '';
        if(is_array($file)){
            if($function_name == 'header_css'){
                foreach($file AS $item){
                    if(strpos($item, 'https://') !== false || strpos($item, 'http://') !== false){
                        $str .= '<link rel="stylesheet" href="'.$item.'" type="text/css" />'."\n";
                    }else{
                        $str .= '<link rel="stylesheet" href="'.base_url().'asset/'.$item.'" type="text/css" />'."\n";
                    }
                }
            }
            if($function_name == 'header_js'){
                foreach($file AS $item){
                    if(strpos($item, 'https://') !== false || strpos($item, 'http://') !== false){
                        $str .= '<script type="text/javascript" src="'.$item.'"></script>'."\n";
                    }else{
                        $str .= '<script type="text/javascript" src="'.base_url().'asset/'.$item.'"></script>'."\n";
                    }
                }
            }
        }else{
            if($function_name == 'header_css'){
                if(strpos($item, 'https://') !== false || strpos($item, 'http://') !== false){
                    $str .= '<link rel="stylesheet" href="'.$item.'" type="text/css" />'."\n";
                }else{
                    $str .= '<link rel="stylesheet" href="'.base_url().'asset/'.$item.'" type="text/css" />'."\n";
                }
            }
            if($function_name == 'header_js'){
                if(strpos($item, 'https://') !== false || strpos($item, 'http://') !== false){
                    $str .= '<script type="text/javascript" src="'.$item.'"></script>'."\n";
                }else{
                    $str .= '<script type="text/javascript" src="'.base_url().'asset/'.$item.'"></script>'."\n";
                }
            }
        }
        echo $str;
    }
}

/*
 * Dynamically add Javascript files to header page
 */
if(!function_exists('footer_js')){
    function footer_js($file=''){
        CssJsConfig('footer_js', $file);
    }
}

/*
 * Dynamically add CSS files to header page
 */
if(!function_exists('footer_css')){
    function footer_css($file=''){
        CssJsConfig('footer_css', $file);
    }
}

/*
 * Dynamically call the js/css loader function for footer
 */
if(!function_exists('CssJsConfig')) {
    function CssJsConfig($function_name, $file){
        $str = '';
        $ci = &get_instance();
        $items = $ci->config->item($function_name);
        if (empty($file)) {
            return;
        }
        if (is_array($file)) {
            if (!is_array($file) && count($file) <= 0) {
                return;
            }
            foreach ($file AS $item) {
                $items[] = $item;
            }
            $ci->config->set_item($function_name, $items);
        } else {
            $str = $file;
            $items[] = $str;
            $ci->config->set_item($function_name, $items);
        }
    }
}

/*
 *  Loading all css and js function in header or footer
 *  like load_resource('header')
 *  like load_resource('footer')
 *
 */
if(!function_exists('load_resource')){
    function load_resource($param = 'header'){
        $str = '';
        $ci = &get_instance();
        if($param == 'footer'){
            $css = $ci->config->item('footer_css');
            $js  = $ci->config->item('footer_js');
        }else{
            $css = $ci->config->item('header_css');
            $js  = $ci->config->item('header_js');
        }
        foreach($css AS $item){
            if(strpos($item, 'https://') !== false || strpos($item, 'http://') !== false){
                $str .= '<link rel="stylesheet" href="'.$item.'" type="text/css" />'."\n";
                $str .= '<link rel="stylesheet" href="'.$item.'" type="text/css" />'."\n";
            }else{
                $str .= '<link rel="stylesheet" href="'.base_url().'asset/'.$item.'" type="text/css" />'."\n";
            }
        }
        $str .= '<link rel="newest stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />'."\n";
        $str .= '<link rel="newest stylesheet" href="'.base_url().'asset/css/main.css" type="text/css" />'."\n";
        foreach($js AS $item){
            if(strpos($item, 'https://') !== false || strpos($item, 'http://') !== false){
                $str .= '<script type="text/javascript" src="'.$item.'"></script>'."\n";
            }else{
                $str .= '<script type="text/javascript" src="'.base_url().'asset/'.$item.'"></script>'."\n";
            }
        }
        return $str;
    }
}


    /*
    *  footer scripts dynamically
    *
    */
    if(!function_exists('add_scripts')){
        function add_scripts($scripts='')
        {
            $ci = &get_instance();
            $footer_scripts = $ci->config->item('footer_scripts');
            $ci->config->set_item('footer_scripts', $footer_scripts.' '.$scripts);
        }
    }

    /*
    *  Load scripts
    *
    */
    if(!function_exists('load_scripts')){
        function load_scripts()
        {
            $ci = &get_instance();
            return $ci->config->item('footer_scripts');
        }
    }



    //Dynamically add Javascript files to header page
    if(!function_exists('add_js')){
        function add_js($file='')
        {
            $str = '';
            $ci = &get_instance();
            $header_js  = $ci->config->item('header_js');
        
            if(empty($file)){
                return;
            }
        
            if(is_array($file)){
                if(!is_array($file) && count($file) <= 0){
                    return;
                }
                foreach($file AS $item){
                    $header_js[] = $item;
                }
                $ci->config->set_item('header_js',$header_js);
            }else{
                $str = $file;
                $header_js[] = $str;
                $ci->config->set_item('header_js',$header_js);
            }
        }
    }
    
    //Dynamically add CSS files to header page
    if(!function_exists('add_css')){
    function add_css($file='')
    {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');
    
        if(empty($file)){
            return;
        }
    
        if(is_array($file)){
            if(!is_array($file) && count($file) <= 0){
                return;
            }
            foreach($file AS $item){   
                $header_css[] = $item;
            }
            $ci->config->set_item('header_css',$header_css);
        }else{
            $str = $file;
            $header_css[] = $str;
            $ci->config->set_item('header_css',$header_css);
        }
    }
    }
    
    if(!function_exists('put_headers')){
    function put_headers()
    {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');
        $header_js  = $ci->config->item('header_js');
    
        foreach($header_css AS $item){
            $str .= '<link rel="stylesheet" href="'.base_url().'css/'.$item.'" type="text/css" />'."\n";
        }
    
        foreach($header_js AS $item){
            $str .= '<script type="text/javascript" src="'.base_url().'js/'.$item.'"></script>'."\n";
        }
    
        return $str;
    }
    }