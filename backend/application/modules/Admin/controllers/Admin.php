<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Admin extends Custom_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model','','TRUE');
    }
    public function static_content(){
        $data = array();
        $this->render_page('static_content', $data);
    }
/*^^^^^^^*/
}