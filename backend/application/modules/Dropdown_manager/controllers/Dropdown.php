<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dropdown extends Custom_Controller {
    public $data = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('dropdown_model','','TRUE');
    }
    public function dropdown_sample($id = ''){
        $this->data['dropdown'] = $this->db->query("SELECT dropdown_slug FROM `dropdown` WHERE dropdown_id = $id")->row();
        $this->load->view('modal_sample', $this->data);
    }
/*^^^^^^^*/
}