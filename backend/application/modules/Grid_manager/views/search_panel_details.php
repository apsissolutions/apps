<div class="col-md-6">
    <div class="box box-info box-solid">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $search_panel->search_panel_name; ?> </h3>
            <?php if(isset($search_panel_details_edit)){ ?>
            <div class=" pull-right">
                <a href="<?php echo base_url('grid_manager/griding/search_panel_view').'/'.$search_panel->search_panel_id; ?>" class="bg-yellow-gradient" style="padding: 3px;"><i class="fa fa-plus"></i> ADD MORE</a>
            </div>
            <?php } ?>
        </div>
        <div class="box-body no-padding">
            <form action="" method="post">
                <div class="col-md-6">
                    <label for="">Input Type</label>
                    <?php
                    $option_data = array(
                        '' => 'Choose Option',
                        'text' => 'Text',
                        'range' => 'Text Range',
                        'dropdown_range' => 'Dropdown Range',
                        'dropdown' => 'Dropdown'
                    );
                    echo form_dropdown('input_type', $option_data, @$search_panel_details_edit->input_type, 'class="form-control multi" id="input_type" required = "required"');
                    ?>
                </div>
                <div class="row"></div>
                <div class="col-md-6 for_text no-display" id="">
                    <label for="">Input Text Type</label>
                    <?php
                    $option_data = array(
                        'text' => 'Text',
                        'number' => 'Number',
                        'email' => 'Email'
                    );
                    echo form_dropdown('input_text_type', $option_data, @$search_panel_details_edit->input_text_type, 'class="form-control" id="input_text_type"');
                    ?>
                </div>
                <div class="row"></div>
                <div class="no-display for_dropdown">
                    <div class="col-md-6">
                        <label for="">Use Dropdown</label>
                        <?php
                        $option_data = array(
                            'text' => 'Text',
                            'number' => 'Number',
                            'email' => 'Email'
                        );
                        $combo_data = array(
                            'selected_value' => isset($search_panel_details_edit->dropdown_slug) ? $search_panel_details_edit->dropdown_slug : ''
                        );
                        echo __combo('dropdown_slug', $combo_data);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label style="padding-top: 10px; padding-bottom: 0; font-weight: 600;">
                                <?php $checked = isset($search_panel_details_edit->dropdown_multiselect) && $search_panel_details_edit->dropdown_multiselect == 1 ? 'checked="checked"' : '';?>
                                <input type="checkbox" name="dropdown_multiselect" value="1" <?php echo $checked; ?>>  Enable Multiselect
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row"></div>
                <hr/>
                <div class="row"></div>
                <div class="common-attr">
                    <div class="col-md-6">
                        <label for="">Input Name</label>
                        <input type="text" name="input_name" value="<?php echo @$search_panel_details_edit->input_name?>" class="form-control">
                    </div>
                    <div class="row"></div>
                    <div class="col-md-6 common-attr">
                        <label for="">Input Class</label>
                        <input type="text" name="input_class" value="<?php echo isset($search_panel_details_edit->input_class) ? $search_panel_details_edit->input_class : 'form-control'; ?>" class="form-control">
                    </div>
                    <div class="col-md-6 common-attr">
                        <label for="">Input ID</label>
                        <input type="text" name="input_id" value="<?php echo @$search_panel_details_edit->input_id?>" class="form-control">
                    </div>
                    <div class="row"></div>
                    <div class="col-md-6 common-attr">
                        <label for="">Label Name</label>
                        <input type="text" name="label_name" value="<?php echo @$search_panel_details_edit->label_name?>" class="form-control">
                    </div>
                    <div class="col-md-6 common-attr">
                        <label for="">Placeholder</label>
                        <input type="text" name="placeholder_name" value="<?php echo @$search_panel_details_edit->placeholder_name?>" class="form-control">
                    </div>
                </div>
                <div class="row"></div>
                <br/>
                <div class="row"></div>
                <div class="col-md-12">
                    <input type="hidden" required name="search_panel_id" value="<?php echo $search_panel->search_panel_id; ?>" class="form-control">
                    <button type="submit" class="btn btn-primary bg-blue-gradient btn-block btn-flat"><i class="fa fa-save"></i> SAVE</button>
                </div>
                <div class="row"></div>
                <br/>
                <div class="row"></div>
            </form>
        </div>
    </div>
</div>
<div class="col-md-6">
    <?php echo $search_panel_details; ?>
</div>
<div class="row"></div>
<script>
    $(document).ready(function () {
        inputChangeStatus();
        $('#dynamic_search').on('submit', function (e) {
            e.preventDefault();
        });
        $('#dynamic_search_button').addClass('disabled');
    });
    $('#input_type').on('change', function () {
        inputChangeStatus();
    });
    function inputChangeStatus() {
        if($('#input_type').val() == 'text'){
            $('.no-display').hide();
            $('.for_text').show();
            $('.common-attr').show();
        }else if($('#input_type').val() == 'dropdown'){
            $('.no-display').hide();
            $('.for_dropdown').show();
            $('.common-attr').show();
        }else if($('#input_type').val() == 'range'){
            $('.no-display').hide();
            $('.for_range').show();
            $('.for_text').show();
        }else if($('#input_type').val() == 'dropdown_range'){
            $('.no-display').hide();
            $('.for_range').show();
            $('.for_dropdown').show();
        }else{
            $('.no-display').hide();
        }
    }
    $('.multi').multiselect({
        includeSelectAllOption: true,
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        templates: {
            filter: '<input class="form-control" type="text" placeholder="Search...">',
        },
        nonSelectedText: 'Choose Option',
        buttonClass: 'form-control',
        buttonWidth: '100%'
    });
    $(".caret").css('float', 'right');
    $(".caret").css('margin', '8px 0');
    $(".multiselect-selected-text").css('float', 'left');
    $(".multiselect-selected-text").css('margin', '0 5px');
</script>
<div class="row"></div>
<?php //dd($search_panel_grid); ?>
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $search_panel->search_panel_name; ?> Details</h3>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <thead class="bg-blue-gradient">
                <tr>
                    <th>ID</th>
                    <th>Label</th>
                    <th>Placeholder</th>
                    <th>Input type</th>
                    <th>Text Input type</th>
                    <th>Dropdown Input Slug</th>
                    <th>Input Name</th>
                    <th>Input Class</th>
                    <th>Input Id</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($search_panel_grid as $grid){ ?>
                <tr>
                    <td><?php echo $grid->search_panel_details_id; ?></td>
                    <td><?php echo $grid->label_name?></td>
                    <td><?php echo $grid->placeholder_name?></td>
                    <td><?php echo $grid->input_type?></td>
                    <td><?php echo $grid->input_text_type?></td>
                    <?php
                    if(!empty($grid->dropdown_slug)){
                        if($grid->dropdown_multiselect == 0){
                            $dr = '(single)';
                        }else{
                            $dr = '(multiple)';
                        }
                    }else{
                        $dr = '';
                    }
                    ?>
                    <td><?php echo $grid->dropdown_slug; ?> <?php echo $dr; ?> </td>
                    <td class="text-red"><?php echo $grid->input_name?></td>
                    <td><?php echo $grid->input_class?></td>
                    <td><?php echo $grid->input_id?></td>
                    <td class="text-center">
                        <a class="text-info" href="<?php echo base_url('grid_manager/griding/search_panel_view').'/'.$search_panel->search_panel_id.'/'.$grid->search_panel_details_id; ?>"><i class="fa fa-pencil"></i></a>
                        &nbsp;&nbsp;
                        <a class="text-danger confirm-link" href="<?php echo base_url('grid_manager/griding/src_panel_det_del').'/'.$search_panel->search_panel_id.'/'.$grid->search_panel_details_id; ?>"><i class="fa fa-close"></i></a>
                    </td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>