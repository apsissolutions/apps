<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-search"></i> <?php echo $search_panel->search_panel_name; ?> </h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="display: block;">
        <form id="dynamic_search" action="" method="post">
            <?php
            foreach ($search_panel_details as $spds){
                if($spds['input_type'] == 'text'){
                    echo '<div class="col-md-4">';
                    echo '<label>' . $spds['label_name'] . '</label>';
                    echo '<input type="'.$spds['input_text_type'].'" name="'.$spds['input_name'].'" value="'.@$$spds['input_name'].'" class="'.$spds['input_class'].'" id="'.$spds['input_id'].'">';
                    echo '</div>';
                }
                if($spds['input_type'] == 'dropdown'){
                    echo '<div class="col-md-4">';
                    echo '<label>' . $spds['label_name'] . '</label>';
                    $combo_data = array(
                        'multiple' => $spds['dropdown_multiselect'],
                        'name' => $spds['input_name']
                    );
                    echo __combo($spds['dropdown_slug'], $combo_data);
                    echo '</div>';
                }
                if($spds['input_type'] == 'range' || $spds['input_type'] == 'dropdown_range'){ ?>
                    <div class="col-md-12">
                        <label><?php echo $spds['label_name']; ?></label>
                        <div class="row"></div>
                        <?php if($spds['input_type'] == 'range'){ ?>
                        <div class="col-md-3 no-padding">
                            <input type="text" name="input_id" value="" class="form-control">
                        </div>
                        <?php }else {
                            echo '<div class="col-md-3 no-padding">';
                            $combo_data = array(
                                'multiple' => $spds['dropdown_multiselect'],
                                'name' => $spds['input_name']
                            );
                            echo __combo($spds['dropdown_slug'], $combo_data);
                            echo '</div>';
                        } ?>
                        <div class="col-md-3 no-padding">
                            <?php
                            $option_data = array(
                                '' => 'Condition',
                                '<' => 'Less than',
                                '<=' => 'Equal OR Less than',
                                '>' => 'Greater than',
                                '>=' => 'Equal OR Greater than',
                                '==' => 'Equal to',
                                '!=' => 'Not Equal to',
                                '><' => 'Between'
                            );
                            $combo_style = 'class="form-control range_condition" data-id="'.$spds['input_name'].'" ';
                            echo form_dropdown('range_type_'.$spds['input_name'], $option_data, $selected = '', $combo_style);
                            ?>
                        </div>
                        <div class="col-md-3 no-padding start_range_val">
                            <input type="text" name="<?php echo 'start_'.$spds['input_name']; ?>" class="form-control" id="" placeholder="Start Range Value">
                        </div>
                        <div class="col-md-3 no-padding no-display end_<?php echo $spds['input_name']; ?>">
                            <input type="text" name="<?php echo 'end_'.$spds['input_name']; ?>" class="form-control" id="" placeholder="End Range Value">
                        </div>
                        <div class="row"></div>
                    </div>
            <?php
                }
            }
            ?>
            <div class="col-md-4">
                <br/>
                <button type="submit" class="btn btn-primary btn-flat btn-block" id="dynamic_search_button"><i class="fa fa-search"></i> Search</button>
            </div>
        </form>
    </div>
</div>
<div class="row"></div>
<script>
    $('.range_condition').on('change', function () {
        var data_id = $(this).data('id');
        if($(this).val() == '><'){
            $('.end_'+data_id).show();
        }else{
            $('.end_'+data_id).hide();
        }
    });
</script>
