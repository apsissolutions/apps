<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Griding extends Custom_Controller {
    public $data = array();
    public function __construct() {
        parent::__construct();
        $this->load->model('griding_model','','TRUE');
    }
    public function search_panel_view($id = '', $details_id = ''){
        if(!empty($id)) {
            $data['search_panel'] = __row("SELECT * FROM search_panel WHERE search_panel_id = $id");
            $data['search_panel_grid'] = __result("SELECT * FROM search_panel_details WHERE search_panel_id = $id");
            if(!empty($details_id))
                $data['search_panel_details_edit'] = __row("SELECT * FROM search_panel_details WHERE search_panel_details_id = $details_id");
            $data['search_panel_details'] = $this->search_panel($id);
            $post = $this->input->post();
            if (!empty($post)) {
                dd($post);
                $insert_data = array(
                    'search_panel_id' => $post['search_panel_id'],
                    'input_type' => isset($post['input_type']) && !empty($post['input_type']) ? $post['input_type'] : '',
                    'input_text_type' => isset($post['input_text_type']) && !empty($post['input_text_type']) ? $post['input_text_type'] : '',
                    'dropdown_slug' => isset($post['dropdown_slug']) && !empty($post['dropdown_slug']) ? $post['dropdown_slug'] : '',
                    'dropdown_multiselect' => isset($post['dropdown_multiselect']) && !empty($post['dropdown_multiselect']) ? $post['dropdown_multiselect'] : '',
                    'input_name' => isset($post['input_name']) && !empty($post['input_name']) ? $post['input_name'] : '',
                    'input_class' => isset($post['input_class']) && !empty($post['input_class']) ? $post['input_class'] : '',
                    'input_id' => isset($post['input_id']) && !empty($post['input_id']) ? $post['input_id'] : '',
                    'label_name' => isset($post['label_name']) && !empty($post['label_name']) ? $post['label_name'] : '',
                    'placeholder_name' => isset($post['placeholder_name']) && !empty($post['placeholder_name']) ? $post['placeholder_name'] : ''
                );
                if(!empty($details_id)){
                    $this->db->where('search_panel_details_id', $details_id);
                    $this->db->update('search_panel_details', $insert_data);
                }else{
                    $this->db->insert('search_panel_details', $insert_data);
                }
                $this->setFlashData('success', 'Entry Successfull! Please Enter another criteria.');
                redirect("grid_manager/griding/search_panel_view/$id");
            }
        }
        if(!empty($data)){
            $this->render_page('search_panel_details', $data);
        }else{
            redirect('home/err_404');
        }
    }
    public function src_panel_det_del($id = '', $details_id = ''){
        $this->db->where('search_panel_details_id', $details_id);
        $this->db->delete('search_panel_details');
        $this->setFlashData('info', 'Entry has been deleted!!');
        redirect("grid_manager/griding/search_panel_view/$id");
    }

    public function search_panel($id = ''){
        $data['search_panel'] = __row("SELECT * FROM search_panel WHERE search_panel_id = $id");
        $htm_data = '';
        if(!empty($id)){
            $data['search_panel_details'] = __result_array("SELECT * FROM search_panel_details WHERE search_panel_id = $id");
            $htm_data = $this->load->view('search_panel', $data, true);
        }
        return $htm_data;
    }

/*^^^^^^^*/
}
