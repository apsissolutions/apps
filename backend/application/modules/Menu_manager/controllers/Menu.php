<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Menu extends Custom_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('menu_model','','TRUE');
    }
    /*=======================================MENU MANAGEMENT====================================*/
    /**
     * @param string $menu_id
     */
    public function menu_list($menu_id = ''){
        if(!empty($menu_id)){
            $data['menu_data'] = $this->menu_model->get_all_menu($menu_id);
        }
        $data['modules'] = $this->db->query("SELECT module_id FROM `module`")->result();
        foreach ($data['modules'] as $module){
            $data['menu_list'][$module->module_id] = $this->getParents($module->module_id);
        }
        $data['current_module'] = $this->module_id;
        $this->render_page('menu_list', $data);
    }

    /**
     * @return string
     */
    public function getParents($module_id = ''){
        $menu_lists = $this->menu_model->get_all_menu(null, '0', $module_id);
        $menu_html = '<ol class="dd-list">';
        foreach($menu_lists as $key => $menu_list){
            $child = $this->getChildren($menu_list->menu_id, $module_id);
            $menu_html .= '<li class="dd-item dd3-item" data-id="'.$menu_list->menu_id.'">';
            $menu_html .= '<div class="dd-handle dd3-handle">Drag</div>';
            $menu_html .= '<div class="dd3-content">';
            $menu_html .= '<i class="'. $menu_list->icon_class .'"></i>&nbsp;&nbsp;' . $menu_list->menu_name;
            if(empty($child)){
                $menu_html .= '<a data-message="" class="btn btn-sm btn-danger pull-right confirm-link" href="'. base_url("menu_manager/menu/menu_delete").'/'.$menu_list->menu_id.'"><i class="fa fa-close"></i></a>';
            }
            $menu_html .= '<a class="btn btn-sm btn-info pull-right" href="'. base_url("menu_manager/menu/menu_list").'/'.$menu_list->menu_id.'"><i class="fa fa-pencil"></i></a>';
            $menu_html .= '</div>';
            $menu_html .= $child;
            $menu_html .= '</li>';
        }
        $menu_html .= '</ol>';
        return $menu_html;
    }

    /**
     * @param string $parent_id
     * @return null|string
     */
    public function getChildren($parent_id = '', $module_id = ''){
        $menu_lists = $this->menu_model->get_all_menu(null, $parent_id, $module_id);
        if(!empty($menu_lists)){
            $menu_html = '<ol class="dd-list">';
            foreach($menu_lists as $key => $menu_list){
                $child = $this->getChildren($menu_list->menu_id, $module_id);
                $menu_html .= '<li class="dd-item dd3-item" data-id="'.$menu_list->menu_id.'">';
                $menu_html .= '<div class="dd-handle dd3-handle">Drag</div>';
                $menu_html .= '<div class="dd3-content">';
                $menu_html .= '<i class="'. $menu_list->icon_class .'"></i>&nbsp;&nbsp;' . $menu_list->menu_name;
                if(empty($child)){
                    $menu_html .= '<a class="btn btn-sm btn-danger pull-right confirm-link" href="'. base_url("menu_manager/menu/menu_delete").'/'.$menu_list->menu_id.'"><i class="fa fa-close"></i></a>';
                }
                $menu_html .= '<a class="btn btn-sm btn-info pull-right" href="'. base_url("menu_manager/menu/menu_list").'/'.$menu_list->menu_id.'"><i class="fa fa-pencil"></i></a>';
                $menu_html .= '</div>';
                $menu_html .= $this->getChildren($menu_list->menu_id, $module_id);
                $menu_html .= '</li>';
            }
            $menu_html .= '</ol>';
            return $menu_html;
        }else{
            return null;
        }
    }

    /**
     *
     */
    public function menu_entry(){
        $post = $this->input->post();
        $menu_data = array(
            'menu_name' => $post['menu_name'],
            'description' => $post['description'],
            'menu_type' => 'Main',
            'parent_menu_id' => 0,
            'module_id' => $post['module_id'],
            'icon_class' => $post['icon_class'],
            'menu_url' => $post['menu_url'],
            'created_by' => $this->user_id,
            'created' => $this->currdatetime,
            'status' => 'Active'
        );
        if(isset($post['menu_id']) && !empty($post['menu_id'])){
            $menu_data = array(
                'menu_name' => $post['menu_name'],
                'description' => $post['description'],
                'module_id' => $post['module_id'],
                'icon_class' => $post['icon_class'],
                'menu_url' => $post['menu_url']
            );
//            dd($menu_data, true);
            $this->db->where('menu_id', $post['menu_id']);
            $this->db->update('menu', $menu_data);
//            dd($this->db->last_query(), true);
        }else{
            $this->db->insert('menu', $menu_data);
        }
//        $menu_id = $this->db->insert_id();
        redirect('menu_manager/menu/menu_list');
    }

    /**
     *
     */
    public function saveMenuOrder(){
        $menus = $this->input->post('data');
//        dd(json_decode($menus), true);
        if(!empty($menus)){
            $menu_arr = [];
            foreach (json_decode($menus) as $key => $menu){
                $menu_arr = [
                    'sort_number' => $key + 1,
                    'parent_menu_id' => 0,
                    'menu_type' => 'Main'
                ];
                $this->db->where('menu_id', $menu->id);
                $this->db->update('menu', $menu_arr);
                if(isset($menu->children) && !empty($menu->children)){
                    $this->menuUpdate($menu->children, $menu->id);
                }
            }
        }
        echo 'saved';
    }

    /**
     * @param array $child_menu_arr
     * @param $parent_id
     */
    public function menuUpdate($child_menu_arr = [], $parent_id){
        foreach ($child_menu_arr as $key => $menu){
            $menu_arr = [
                'sort_number' => $key + 1,
                'parent_menu_id' => $parent_id,
                'menu_type' => 'Sub'
            ];
            $this->db->where('menu_id', $menu->id);
            $this->db->update('menu', $menu_arr);
            if(isset($menu->children) && !empty($menu->children)){
                $this->menuUpdate($menu->children, $menu->id);
            }
        }
    }

    /**
     * @param string $menu_id
     */
    public function menu_delete($menu_id = ""){
        if(!empty($menu_id)){
            $this->db->where('menu_id', $menu_id);
            $this->db->delete('privilege_menu');
            $this->db->where('menu_id', $menu_id);
            $this->db->delete('menu');
        }
        $this->setFlashData('danger', '<h4 class="">Menu has been Deleted</h4>');
        redirect('menu_manager/menu/menu_list');
    }

    /*=======================================LEVEL-WISE MENU PRIVILEDGE====================================*/
    /**
     * @param string $module_id
     * @param string $level_id
     */
    public function get_menu_for_level($module_id = '', $level_id = ''){
        $post = $this->input->post();
        $data = array();
        if(isset($post['module_id']) && !empty($post['module_id'])){
            $module_id = $post['module_id'];
        }
        if(isset($post['user_level_id']) && !empty($post['user_level_id'])){
            $level_id = $post['user_level_id'];
        }
        if ($level_id != '') {
            $current_permitted_menu = $this->menu_model->get_level_menu_list($level_id);
            $get_menu_info = $this->main_model->get_menu_model(NULL, $module_id);
            $array_str = '';
            $array_str .= '<ul style="padding-left: 0;">';
            foreach ($get_menu_info as $menu_item) {
                $array_str .= "<li style='list-style: none;'>";
                $array_str .= "<span>";
                $array_str .= "<label>";
                $array_str .= "<input name='menu_id[]' type='checkbox' value='" . $menu_item['menu_id'] . "' " . (in_array($menu_item['menu_id'], $current_permitted_menu) ? 'checked' : '') . " />";
                $array_str .= "&nbsp;&nbsp;<i class='".$menu_item['icon_class']."'></i>&nbsp;".$menu_item['menu_name'];
                $array_str .= "</label>";
                $array_str .= "</span>";
                $array_str .= "</li>";
                $array_str .= $this->create_menu_list_all($menu_item['menu_id'], $current_permitted_menu);
            }
            $array_str .= '</ul>';
            $data['menu_list_array'] = $array_str;
            $data['selected_level'] = $level_id;
            $data['selected_module'] = $module_id;
        }
        $this->render_page('set_level_access', $data);
    }
    /**
     * @param $menu_id
     * @param $permitted_array
     * @return string
     */
    public function create_menu_list_all($menu_id, $permitted_array){
        $result = $this->main_model->get_menu($menu_id);
        $p =  "";
        if(!empty($result)){
            $p =  "<ul>";
            foreach($result as $item){
                if ($item['Count'] > 0) {
                    $p .= "<li style='list-style: none;'>";
                    $p .= "<span>";
                    $p .= "<label>";
                    $p .= "<input name='menu_id[]' type='checkbox' value='".$item['menu_id']."' ".(in_array($item['menu_id'], $permitted_array)?'checked':'')." />";
                    $p .= "&nbsp;&nbsp;<i class='".$item['icon_class']."'></i>&nbsp;".$item['menu_name'];
                    $p .= "</label>";
                    $p .= "</span>";
                    $p .= $this->create_menu_list_all($item['menu_id'],$permitted_array);
                    $p .= "</li>";
                } elseif($item['Count'] == 0) {
                    $p .= "<li style='list-style: none;'>";
                    $p .= "<span>";
                    $p .= "<label>";
                    $p .= "<input name='menu_id[]' type='checkbox' value='".$item['menu_id']."' ".(in_array($item['menu_id'], $permitted_array)?'checked':'')." />";
                    $p .= "&nbsp;&nbsp;<i class='".$item['icon_class']."'></i>&nbsp;".$item['menu_name'];
                    $p .= "</label>";
                    $p .= "</span>";
                    $p .= "</li>";
                } else;
            }
            $p .= "</ul>";
        }
        return $p;
    }
    /**
     *
     */
    public function set_menu_access_for_level(){
        $selected_menu_array = $this->input->post('menu_id');
        $level_id = $this->input->post('level_id');
        $module_id = $this->input->post('module_id');
        $this->db->query("DELETE FROM privilege_menu 
                WHERE user_level_id = '$level_id' 
                AND privilege_menu.menu_id IN (SELECT menu.menu_id FROM menu WHERE menu.module_id = $module_id)");
        foreach ($selected_menu_array as $menu_id) {
            $data[] = array(
                    'menu_id' => $menu_id ,
                    'user_level_id' => $level_id
                );
        }
        $this->db->insert_batch('privilege_menu', $data);
        $this->setFlashData('<h4 class="text-info">Level-wise Menu Priviledge Has been Successfully Intigrated.</h4>');
        $redirect_url = 'menu_manager/menu/get_menu_for_level/'.$module_id.'/'.$level_id;
        redirect($redirect_url);
    }

/*^^^^^^^*/
}