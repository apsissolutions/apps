<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Nirjhar
 * Date: 8/19/2017
 * Time: 1:20 PM
 */
class Menu_model extends Custom_model {
    public function __construct(){
        parent::__construct();
    }
    public function get_all_menu( $menu_id = NULL, $parent_id = null, $module_id = ''){
        $condition = '';
        if($parent_id != NULL)
            $condition .= " AND menu.parent_menu_id = $parent_id";
        if($menu_id != NULL)
            $condition .= " AND menu.menu_id = $menu_id";
        if($module_id != NULL)
            $condition .= " AND menu.module_id = $module_id";

        $sql = "SELECT menu.*, module_name 
            FROM menu 
            INNER JOIN module ON module.module_id = menu.module_id
            WHERE 1 $condition
            ORDER BY menu.sort_number ASC";
//        dd($sql, true);
        $query = $this->db->query($sql);

        if(!empty($menu_id)){
            return $query->row();
        }else{
            return $query->result();
        }
    }
    public function get_level_menu_list($level_id){
        $data = array();
        $query = $this->db->query("SELECT privilege_menu.menu_id, privilege_menu.user_level_id, menu.module_id
            FROM privilege_menu INNER JOIN menu ON privilege_menu.menu_id = menu.menu_id
            WHERE privilege_menu.user_level_id = '$level_id'");
        foreach($query->result_array() as $item){
            $data[] = $item['menu_id'];
        }
        return $data;
    }
}