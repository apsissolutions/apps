<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends Custom_model {
    function __construct(){
        parent::__construct();
        $this->user_id = $this->session->userdata('USER_ID');
    }
    public function get_table($table_name) {
        if(empty($table_name)){
            return;
        }
        $query = $this->db->query("DESCRIBE $table_name");
        return $query->result_array();
    }
    public function get_master_table(){
        $query = $this->db->query("SELECT master_entry_table_id,master_entry_table_name FROM master_entry_table");
        return $query->result_array();
    }
    public function get_previous_table($master_entry_table_name, $mask_title){
        if(empty($master_entry_table_name)){
            return;
        }
        $query = $this->db->query("SELECT field_name,`sql`,comma_separated_value,hide_from_grid,self_filtering,hide_from_input,input_type,required_field,label_name,placeholder,search FROM master_entry WHERE master_entry_table_name = '$master_entry_table_name' AND master_entry_title='$mask_title'");
        return $query->result_array();
    }
    public function table_content($fields, $lab, $place,$sql,$hide_from_grid,$self_filtering,$hide_from_input,$comma_separated_value,$input_type,$required_type,$master_entry_table_name,$request_for,$master_entry_title,$search){
        //print_r($sql);
        if($request_for != 'show'){
            foreach($fields as $column){
                $query = $this->db->query("DESCRIBE $master_entry_table_name $column");
                $rec[]=$query->result_array();
            }
            //print_r($fields);
            $this->db->query("DELETE FROM master_entry WHERE master_entry_table_name='$master_entry_table_name' AND master_entry_title='$master_entry_title'");
            foreach($rec as $val){
                foreach($val as $val2){
                    $label = $lab[$val2['Field']];
                    $placeholder = $place[$val2['Field']]; //input type will be var/int/dou/tex/num/........
                    $sql_str = $sql[$val2['Field']];
                    $comma_val = $comma_separated_value[$val2['Field']];
                    $hide_in_grid = isset($hide_from_grid[$val2['Field']])?1:0;
                    $active_filter = isset($self_filtering[$val2['Field']])?1:0;
                    $hide_in_input = isset($hide_from_input[$val2['Field']])?1:0;
                    $inpt_type = $input_type[$val2['Field']];
                    $field_name = $val2['Field'];
                    $type = empty($inpt_type) ? substr($val2['Type'], 0, 3) : $inpt_type;
                    $null = $val2['Null'];
                    $key = $val2['Key'];
                    $default = $val2['Default'];
                    $extra = $val2['Extra'];
                    $include_search = (isset($search[$val2['Field']]) && $search[$val2['Field']]!='')?1:0;
                    $validation = isset($required_type[$val2['Field']])?$required_type[$val2['Field']]:'';
                    $this->db->query("INSERT INTO master_entry "
                        . "SET "
                        . "master_entry_table_name='$master_entry_table_name', "
                        . "field_name='$field_name', "
                        . "field_key='$key', "
                        . "input_type='$type', "
                        . "required_field='$validation', "
                        . "`sql`='$sql_str',"
                        . "comma_separated_value='$comma_val',"
                        . "hide_from_grid='$hide_in_grid', "
                        . "self_filtering='$active_filter', "
                        . "hide_from_input='$hide_in_input', "
                        . "label_name='$label', "
                        . "placeholder='$placeholder', "
                        . "search='$include_search', "
                        . "created=NOW(),"
                        . "master_entry_title='$master_entry_title'");

                }
            }
        }
        $q = $this->db->query("SELECT * FROM master_entry WHERE master_entry_table_name = '$master_entry_table_name' AND master_entry_title='$master_entry_title' ORDER BY sorting");
        return $q->result_array();
    }
    public function combo_table($foreign_table){
        $q = $this->db->query("SELECT * FROM $foreign_table ");
        return $q->result_array();
    }
    public function array_result($sql){
        $query = $this->db->query($sql);
        return array($query->list_fields(), $query->result_array());
    }
    public function get_array_result($sql){
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function get_table_info($table_name){
        $query = $this->db->query("SELECT * FROM master_entry WHERE master_entry_table_name = '$master_entry_table_name' ORDER BY sorting");
        return $query->result_array();
    }
    public function reorder_entry_element($data){
        foreach ($data as $value) {
            $split_value = explode("=", $value);
            $view_name = $split_value[0];
            $field_name = $split_value[1];
            $sort_number = $split_value[2];
            $sql = "UPDATE master_entry SET sorting='$sort_number' WHERE master_entry_title='$view_name' AND field_name='$field_name'";
            $this->db->query($sql);
        }
    }
    public function get_permitted_level_list($user_id=NULL){
        $condition = $user_id == NULL?"":" WHERE pl.user_id='$user_id' ";
        $sql = "SELECT
        pl.user_id,
        GROUP_CONCAT(pl.user_level_id) AS level_id_list,
        GROUP_CONCAT(user_level.user_level_name) AS level_id_list_name,
        `user`.first_name
        FROM
        privilege_level AS pl
        INNER JOIN `user` ON pl.user_id = `user`.user_id
        INNER JOIN user_level ON pl.user_level_id = user_level.user_level_id
        $condition
        GROUP BY pl.user_id
        ORDER BY pl.user_id";

        $query = $this->db->query($sql);
        return $query->result_array();
    }
    public function join_insertion($join_count,$join_table_name,$join_field_name,$join_relation_field_name,$join_sql,$join_label,$tbl_name,$master_entry_title){
        foreach ($join_count as $value) {
            $jn_tbl_name = $join_table_name[$value];
            $jn_fld_name = $join_field_name[$value];
            $jn_rln_fld_name = $join_relation_field_name[$value];
            $jn_sql = $join_sql[$value];
            $jn_lbl = $join_label[$value];

            $data = array(
                'master_entry_table_name' => $tbl_name ,
                'field_name' => $jn_fld_name ,
                'input_type' => 'select',
                'required_field' => 'required',
                'sql' => $jn_sql,
                'hide_from_grid' => 1,
                'hide_from_input' => 0,
                'label_name' => $jn_lbl,
                'join_with' => $jn_tbl_name,
                'join_with_fk_name' => $jn_rln_fld_name,
                'created_by' => $this->user_id,
                'created' => date('Y-m-d H:i:s'),
                'master_entry_title' => $master_entry_title
            );

            $this->db->insert('master_entry', $data);
        }
    }
    public function get_extra_relation_with_table($master_table_name,$view_name){
        $sql = "SELECT master_entry.master_entry_table_name,
        master_entry.field_name,
        master_entry.input_type,
        master_entry.required_field,
        master_entry.`sql`,
        master_entry.comma_separated_value,
        master_entry.hide_from_grid,
        master_entry.hide_from_input,
        master_entry.sorting,
        master_entry.label_name,
        master_entry.placeholder,
        master_entry.field_key,
        master_entry.created_by,
        master_entry.created,
        master_entry.updated_by,
        master_entry.updated,
        master_entry.`status`,
        master_entry.join_with,
        master_entry.join_with_fk_name
        FROM master_entry
        WHERE master_entry.join_with IS NOT NULL AND
        master_entry.master_entry_table_name = '$master_table_name' AND master_entry_title = '$view_name'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
//    public function get_details($tbl_name, $id){
//        $sql = $this->db->query("SELECT details_view_sql FROM master_entry_table WHERE master_entry_table_name='$tbl_name'")->row()->details_view_sql;
//        $full_sql = $sql." WHERE `$tbl_name`.".$tbl_name."_id = '$id'";
//        $result = $this->db->query($full_sql)->row();
//        return $result;
//    }
    public function get_mask_table(){
        $query = $this->db->query("SELECT master_entry_table_id,master_entry_table_name,master_entry_title FROM master_entry_table");
        return $query->result_array();
    }
    public function get_original_table_name($mask_title){
        $sql = "SELECT master_entry_table_name FROM master_entry_table WHERE master_entry_title ='$mask_title'";
        $table_name = $this->db->query($sql)->row()->master_entry_table_name;
        return $table_name;
    }
    public function get_upload_directory($view_name){
        $this->db->select('file_upload_path');
        $this->db->where('master_entry_title', $view_name);
        $query = $this->db->get('master_entry_table');
        return $query->row()->file_upload_path;
    }
    public function get_table_field_from_master_entry($view_name){
        $this->db->select('field_name');
        $this->db->where('master_entry_title',$view_name);
        $query = $this->db->get('master_entry');
        return $query->result_array();
    }
    public function get_module_url($module_id){
        $get_url = $this->db->query("SELECT home_url FROM module WHERE module_id = '$module_id'")->row()->home_url;
        return $get_url;
    }
    public function create_master_table($table_name){
        $sql = "CREATE TABLE `$table_name` (
        `".$table_name."_id`  int(10) NOT NULL AUTO_INCREMENT ,
        `".$table_name."_name`  varchar(250) NOT NULL ,
        `description`  text NOT NULL ,
        `created_by`  int(10) NULL ,
        `created`  TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
        `updated_by`  int(10) NULL ,
        `updated`  TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP ,
        `status`  enum('Active', 'Inactive') NULL DEFAULT 'Active' ,
        PRIMARY KEY (`".$table_name."_id`)
        )";
        $this->db->query($sql);
    }
    public function user_information($user_id = ''){
        $sql = "SELECT user.* FROM user WHERE user.user_id = '$user_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}

