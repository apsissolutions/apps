<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Priviledge_Model extends Custom_model {
    /**
     * Partner_Model constructor.
     */
    function __construct(){
        parent::__construct();
    }

    // location type 7 for Region
    function get_region($cond =''){
        $regions = $this->db->query("SELECT
            DISTINCT _locations.id,
            _locations.slug
            FROM
            _locations
            INNER JOIN distributorspoint ON distributorspoint.region = _locations.id
            INNER JOIN routes ON routes.dpid = distributorspoint.id
            WHERE
            _locations.ltype = 7 AND _locations.stts = 1 AND distributorspoint.id <>334 $cond ORDER BY  _locations.slug ")->result_array();
        return $regions;
    }

    //location type 8 for area
    function get_area($cond =''){
        $areas = $this->db->query("SELECT
            DISTINCT _locations.id,
            _locations.slug,
            _locations.parent AS region
            FROM
            _locations
            INNER JOIN distributorspoint ON distributorspoint.area = _locations.id
            INNER JOIN routes ON routes.dpid = distributorspoint.id
            WHERE
            _locations.ltype = 8 AND _locations.stts = 1 AND distributorspoint.id <>334 $cond ORDER BY  _locations.slug ")->result_array();
        return $areas;
    }

    // LOCATION TYPE 9 FOR TERRITORY
    function get_territory($cond = ''){
        $territory = $this->db->query("SELECT
		DISTINCT _locations.id,
		_locations.slug,
		distributorspoint.dsid,
		distributorspoint.area,
		distributorspoint.region
		FROM
		_locations
		INNER JOIN distributorspoint ON distributorspoint.territory = _locations.id
        INNER JOIN routes ON routes.dpid = distributorspoint.id
		WHERE
		_locations.ltype = 9 AND
		_locations.stts = 1 AND distributorspoint.id <>334 $cond ORDER BY  _locations.slug")->result_array();
        return $territory;
    }

    function get_house($cond = ''){
        $house = $this->db->query("SELECT
            DISTINCT company.region,
            company.area,
            company.id,
            company.`name`
            FROM
            company
            INNER JOIN distributorspoint ON distributorspoint.dsid = company.id 
            INNER JOIN routes ON routes.dpid = distributorspoint.id
            WHERE distributorspoint.id <>334 AND company.stts = 1 $cond ORDER BY  company.`name` ")->result_array();
        return $house;
    }

    function get_point($cond = ''){
       $points = $this->db->query("SELECT
            DISTINCT distributorspoint.dsid,
            distributorspoint.region,
            distributorspoint.area,
            distributorspoint.territory,
            distributorspoint.id,
            distributorspoint.`name`
            FROM
            distributorspoint 
            INNER JOIN routes ON routes.dpid = distributorspoint.id
            WHERE distributorspoint.id <> 334 $cond ORDER BY  distributorspoint.`name`")->result_array();
       return $points;
    }

    function get_route(){
        $routes = $this->db->query("SELECT
            routes.id,
            routes.dpid,
            distributorspoint.name point,
            routes.number,
            section_days.slug,
            CONCAT(distributorspoint.name,'-',routes.number,section_days.slug) as slug
            FROM
            routes
            INNER JOIN section_days ON routes.section = section_days.section
            INNER JOIN distributorspoint ON distributorspoint.id = routes.dpid
            WHERE
            routes.stts = 1")->result_array();
        return $routes;
    }

    public function get_tsa_location($cond){
        $tsa = $this->db->query("SELECT
                    DISTINCT
                    tsa_location.territory,
                    tsa_location.id,
                    tsa_location.`name`
                    FROM
                    tsa_location
                    #INNER JOIN routes ON tsa_location.id = routes.tsa_id
                    #INNER JOIN distributorspoint ON tsa_location.dsid = distributorspoint.dsid
                    #INNER JOIN _locations t ON t.id = tsa_location.territory
                    WHERE
                    tsa_location.stts = 1 $cond ")->result_array();
        return $tsa;
    }

    function get_promo_id($permit_type,$permit_id){

        if($permit_type == 'urc'){
            $table = 'pp_urc';
            $select = 'promotion_id,urc_name name';
            $where = 'pp_urc_id';

        }elseif ($permit_type == 'cdr'){
            $table = 'pp_cdr';
            $select = 'promotion_id,cdr_name name';
            $where = 'pp_cdr_id';
        }elseif ($permit_type == 'pc'){
            $table = 'pp_pc';
            $select = 'promotion_id,pc_name name';
            $where = 'pp_pc_id';
        }else{
            $table = 'pp_vol_config';
            $select = 'promotion_id, volume_name name';
            $where = 'pp_vol_config_id';
        }

        $join_condition = "
                LEFT JOIN pp_promotion ON $table.promotion_id = pp_promotion.pp_promotion_id";

        $sql = "SELECT pp_promotion.promotion_name,$select FROM $table  $join_condition WHERE $where = $permit_id AND $table.status = 'active'";

        return $this->db->query($sql)->row();


    }
}
